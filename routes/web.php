<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
// $router->get('login/','LoginController@login');
$router->get('login/{username}/{password}','LoginController@login');
$router->get('bagan/{base}', ['uses' => 'GlobalFunctionController@generateBracket']);
$router->get('allJurorGrand', ['uses' => 'GlobalFunctionController@showAllJurorAndGrandJuror']);
$router->post('editJurorGrand', ['uses' => 'GlobalFunctionController@editJurorGrandJuror']);
$router->get('showJurorGrand', ['uses' => 'GlobalFunctionController@showByIdJurorGrandJuror']);


// isian body generate bagan : championship Id, class id, scheme name
$router->post('competition/generate', ['uses' => 'CompetitionController@generateCompetition']);

$router->get('random/{total}', ['uses' => 'CompetitionController@RandomNumbers']);
$router->post('setWin', ['uses' => 'CompetitionController@setWinner']);



// $router->group(['middleware' => 'auth', 'prefix' => 'api'], function () use ($router) {
$router->group(['prefix' => 'api'], function () use ($router) {
    
    // Arena API
    $router->post('arena/', [ 'uses' => 'ArenaController@store']);
    $router->get('arena/{id}', ['uses' => 'ArenaController@showById']);
    // $router->get('arena', ['uses' => 'ArenaController@showAllArena']);
    $router->get('arena', ['uses' => 'ArenaController@showAllArena']);
    $router->post('arena/{id}', ['uses' => 'ArenaController@delete']);
    $router->put('arena/{id}', ['uses' => 'ArenaController@update']);

    // Championship API
    $router->post('championship', 'ChampionshipController@store');
    $router->get('championship/{id}', ['uses' => 'ChampionshipController@showById']);
    $router->get('championship', 'ChampionshipController@showChampionshipBy');
    $router->get('championship/del/{id}', ['uses' => 'ChampionshipController@delete']);
    $router->post('championship/put/{id}', ['uses' => 'ChampionshipController@update']);

    // Participant API
    $router->post('participant', 'ParticipantController@store');
    $router->post('participant/save', 'ParticipantController@saveParticipant');
    /*
    Routes get participant -> api/participant/?name=(nama peserta)&class=(id class)&contingent=(id contingent)&gender=(gender)&sortBy=..
    */
    $router->get('participant', ['uses' => 'ParticipantController@showParticipantBy']);

    $router->get('participant/id/{id}', ['uses' => 'ParticipantController@showById']);
    $router->get('participant/name/{name}', ['uses' => 'ParticipantController@showByName']);
    $router->get('participant/show/all', 'ParticipantController@showAllParticipant');
    $router->get('participant/del/{id}', ['uses' => 'ParticipantController@delete']);
    $router->post('participant/put/{id}', ['uses' => 'ParticipantController@update']);

    // Contingent API
    $router->post('contingent', ['as' => 'createContingent','uses' => 'ContingentController@store']);
    $router->post('contingent/official', ['as' => 'createContingent','uses' => 'ContingentController@save']);
    $router->get('contingent/id/{id}/{championship}', ['uses' => 'ContingentController@showById']);
    $router->get('contingent', 'ContingentController@showContingentBy');
    $router->get('contingent/championship/{id}', 'ContingentController@showByChampionshipId');
    $router->get('contingent/del/{id}', ['uses' => 'ContingentController@delete']);
    $router->post('contingent/put/{id}', ['uses' => 'ContingentController@update']);

    // Official API
    $router->post('official', 'OfficialController@store');
    $router->get('official/{id}', ['uses' => 'OfficialController@showById']);
    $router->get('official', 'OfficialController@showAllOfficial');
    $router->delete('official/{id}', ['uses' => 'OfficialController@delete']);
    $router->put('official/{id}', ['uses' => 'OfficialController@update']);

    // Class API
    $router->post('class', 'ClassyController@store');
    $router->get('class/{id}', ['uses' => 'ClassyController@showById']);
    $router->get('class/group/{id}', [ 'uses' => 'ClassyController@showClassByGroupId']);
    $router->get('class', [ 'uses' => 'ClassyController@showAllClass']);
    $router->get('class/gender/{gender}', [ 'uses' => 'ClassyController@showClassByGender']);
    $router->get('class/name/{name}', [ 'uses' => 'ClassyController@showClassByName']);
    $router->delete('class/{id}', ['uses' => 'ClassyController@delete']);
    $router->put('class/{id}', ['uses' => 'ClassyController@update']);

    // Category API
    $router->post('category', 'CategoryController@store');
    $router->get('category/{id}', ['uses' => 'CategoryController@showById']);
    $router->get('category', 'CategoryController@showAllCategory');
    $router->delete('category/{id}', ['uses' => 'CategoryController@delete']);
    $router->put('category/{id}', ['uses' => 'CategoryController@update']);

    // Group API
    $router->post('group', 'GroupController@store');
    $router->get('group/{id}', ['uses' => 'GroupController@showById']);
    $router->get('group', 'GroupController@showAllGroup');
    $router->delete('group/{id}', ['uses' => 'GroupController@delete']);
    $router->put('group/{id}', ['uses' => 'GroupController@update']);


    // Grand Juror API
    $router->post('grandJuror', 'GrandJurorController@store');
    $router->get('grandJuror/{id}', ['uses' => 'GrandJurorController@showById']);
    $router->get('grandJuror', 'GrandJurorController@showAllGrandJuror');
    $router->get('grandJuror/del/{id}', ['uses' => 'GrandJurorController@delete']);
    $router->post('grandJuror/put/{id}', ['uses' => 'GrandJurorController@update']);

    //Competition API
    $router->post('competition', 'CompetitionController@store');
    $router->post('competition/setSchedule', 'CompetitionController@setSchedule');
    $router->put('competition/participant/insert', ['uses' => 'CompetitionController@insertParticipant'] );
    $router->get('competition', ['uses' => 'CompetitionController@showCompetitionBy']);
    $router->get('competition/{id}', ['uses' => 'CompetitionController@showById']);
    $router->get('competition/show/all', 'CompetitionController@showAllCompetition');
    $router->get('competition/show/scheduled', 'CompetitionController@showScheduledCompetition');
    $router->get('competition/show/schedule', 'CompetitionController@showSchedule');
    $router->get('competition/show/scheduled/list', 'CompetitionController@showTime');
    $router->get('competition/show/monitoring', 'CompetitionController@showCompetitionMonitoring');
    $router->get('competition/show/monitoringByDate', 'CompetitionController@showCompetitionMonitoringByDate');//@kaha
    $router->get('competition/show/monitoring/list', 'CompetitionController@showRoundAndTime');
    $router->get('competition/show/scheduleSession/list', 'CompetitionController@showRoundAndTimeByDate'); //@kaha
    $router->get('competition/show/round/list', 'CompetitionController@showRound');
    $router->get('competition/scheme/{scheme}', 'CompetitionController@showCompetitionScheme');
    $router->get('competition/schemeAll/All', 'CompetitionController@showCompetitionSchemeAll');
    $router->delete('competition/{id}', ['uses' => 'CompetitionController@delete']);
    $router->post('competition/delete/schedule', ['uses' => 'CompetitionController@deleteSchedule']);
    $router->put('competition/edit/schedule', 'CompetitionController@updateSchedule');
    $router->put('competition/{id}', ['uses' => 'CompetitionController@update']);

    //Juror API
    $router->post('juror', 'JurorController@store');
    $router->get('juror/{id}', ['uses' => 'JurorController@showById']);
    $router->get('juror/competition/{id}', ['uses' => 'JurorController@showCompetitionJuror']);
    $router->get('juror', 'JurorController@showAllJuror');
    $router->get('juror/del/{id}', ['uses' => 'JurorController@delete']);
    $router->post('juror/put/{id}', ['uses' => 'JurorController@update']);

    // Statistic API
    $router->post('statistic', 'StatisticController@store');
    $router->get('statistic/{id}', ['uses' => 'StatisticController@showById']);
    $router->get('statistic', 'StatisticController@showAllStatistic');
    $router->delete('statistic/{id}', ['uses' => 'StatisticController@delete']);
    $router->put('statistic/{id}', ['uses' => 'StatisticController@update']);

    //Report API
    $router->get('reportMedal', ['uses' => 'ContingentController@getContingentMedal']);
    $router->get('getClassChampion', ['uses' => 'CompetitionController@getClassChampion']);
    $router->get('reportWeight/{id}', ['uses' => 'CompetitionController@reportWeight']);
    
    // Scheme API
    $router->post('scheme', 'SchemeController@store');
    $router->get('scheme/getId/{id}', ['uses' => 'SchemeController@showById']);
    $router->get('scheme', 'SchemeController@showAllScheme');
    $router->get('scheme/name/', 'SchemeController@showAllSchemeName');
    $router->delete('scheme/{id}', ['uses' => 'SchemeController@delete']);
    $router->put('scheme/{id}', ['uses' => 'SchemeController@update']);

    //Scoring API
    $router->post('insertScore', 'ScoringController@setScoreByJuror');
    $router->post('startScoring', 'ScoringController@startCompetition');
    $router->post('stopScoring', 'ScoringController@stopCompetition');
    $router->post('finishScoring', 'ScoringController@finishCompetition');
    $router->post('monitoringCompetition', 'ScoringController@monitoringCompetition');
    $router->get('showScore', 'ScoringController@showScore');
    // $router->get('scheme/getId/{id}', ['uses' => 'ScoringController@showById']);
    // $router->get('scheme', 'ScoringController@showAllScore');
    // $router->get('scheme/name/', 'ScoringController@showAllScoreName');
    // $router->delete('scheme/{id}', ['uses' => 'ScoringController@delete']);
    // $router->put('scheme/{id}', ['uses' => 'ScoringController@update']);
});


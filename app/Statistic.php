<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    protected $table = 'statistic';
    public $timestamps = false;
    protected $primaryKey = 'statisticId';
}
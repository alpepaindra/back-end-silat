<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scheme extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    protected $table = 'competition_scheme';
    public $timestamps = false;
    protected $primaryKey = 'schemeId';
}
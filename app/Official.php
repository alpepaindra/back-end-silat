<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Official extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    protected $table = 'official';
    public $timestamps = false;
    protected $primaryKey = 'officialId';

    public function contingent(){
        return $this->belongsTo('App\Contingent');
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrandJuror extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    protected $table = 'grand_juror';
    public $timestamps = false;
    protected $primaryKey = 'grandJurorId';
}
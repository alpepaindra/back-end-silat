<?php

namespace App\Http\Controllers;

use App\DetailCompetition;
use App\Competition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
ini_set('memory_limit', '-1');

class ScoringController extends Controller{
    
    public function setScore(Request $request){
        /*
        nilai
        jurrorId
        competitionId
        */
        $check = [];
        $check['score'] = $request->input('score');
        $check['competitionId'] = $request->input('competitionId');
        $check['jurorId'] = $request->input('jurorId');
        print_r($check);

        if(!empty($check)){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }



    }

    /*
        nilai
        competitionId
        red / blue
        jurorId
        round
        */
    public function setScoreByJuror(Request $request){
        

        $score = $request->input('score');
        $competitionId = $request->input('competitionId');
        $side = $request->input('side');
        $jurorId = $request->input('jurorId');
        $round = $request->input('round');
        // $competitionScoreId = (object)[];
        switch ($score) {
            case "1":
                $score = 1;
                $scoreView = "1";
                $status = "insert";

                break;
            case "2":
                $score = 2;
                $scoreView = "2";
                $status = "insert";
                break;
            case "3":
                $score = 3;
                $scoreView = "3";
                $status = "insert";
                break;
            case "1+1":
                $score = 2;
                $scoreView = "1+1";
                $status = "insert";
                break;
            case "1+2":
                $score = 3;
                $scoreView = "1+2";
                $status = "insert";
                break;
            case "1+3":
                $score = 4;
                $scoreView = "1+3";
                $status = "insert";
                break;
            case "-1":
                $score = -1;
                $scoreView = "-1";
                $status = "insert";
                break;
            case "-2":
                $score = -2;
                $scoreView = "-2";
                $status = "insert";
                break;
            case "-5":
                $score = -5;
                $scoreView = "-5";
                $status = "insert";
                break;
            case "-10":
                $score = -10;
                $scoreView = "-10";
                $status = "insert";
                break;
            case "del":
                $status = "delete";
                break;
            case "*": 
                $status = "reset";

            
        }

        if($side == 'red'){
            $participant = DB::table('competition')->select('redCorner')->where('competitionId', $competitionId)->first();
            $competitionScoreId = DB::table('participant_score')->select('competitionScoreId')->where('participantId',$participant->redCorner)->where('competitionId', $competitionId)->get();
            // print_r($competitionScoreId[$round-1]->competitionScoreId);
            $detailCompetitionId = DB::table('competition_score')->select('detailCompetitionId')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->first();
            $redScore = DB::table('detail_competition')->select('redScore')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->first();
            if($status == "insert"){
                if(!is_null($redScore)){
                    $redScore = $score + $redScore->redScore;
                }else{
                    $redScore = $score;
                }
                DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['redScore' => $redScore]);
            }


        }elseif ($side == 'blue') {
            $participant = DB::table('competition')->select('blueCorner')->where('competitionId', $competitionId)->first();
            $competitionScoreId = DB::table('participant_score')->select('competitionScoreId')->where('participantId',$participant->blueCorner)->where('competitionId', $competitionId)->get();
            $detailCompetitionId = DB::table('competition_score')->select('detailCompetitionId')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->first();
            $blueScore = DB::table('detail_competition')->select('blueScore')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->first();
            if($status == "insert"){
                if(!is_null($blueScore)){
                    $blueScore = $score + $blueScore->blueScore;
                }else{
                    $blueScore = $score;
                }
                DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['blueScore' => $blueScore]);
            }
            
        }

        $jurorNumber = DB::table('scoring')->select('jurorNumber')->where('jurorId', $jurorId)->where('competitionId', $competitionId)->first();
        $totalScore = DB::table('competition_score')->select('totalScore')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->first();
        if($status == "insert"){
            if(!is_null($totalScore)){
                $totalScore = $score + $totalScore->totalScore;
            }else{
                $totalScore = $score;
            }
        }

        switch ($jurorNumber->jurorNumber) {
            case 1:
                $totalJurorScoreValue = 0;
                $totalJurorScore = DB::table('competition_score')->select('scoreByJuror1', 'scoreByJuror1Value')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->first();
                
                if($status == "insert"){
                    if(!is_null($totalJurorScore->scoreByJuror1)){
                        // var_dump($totalJurorScore->scoreByJuror1);
                        $totalJurorScoreView = $totalJurorScore->scoreByJuror1.",".$scoreView;
                        
                        $totalJurorScoreValue = $totalJurorScore->scoreByJuror1Value + $score;
                    }else{
                        $totalJurorScoreView = $scoreView;
                        $totalJurorScoreValue = $score;
                    }
                    
                    DB::table('competition_score')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->update(['scoreByJuror1' => $totalJurorScoreView, 'scoreByJuror1Value' => $totalJurorScoreValue, 'totalScore' => $totalScore ]);
                }elseif($status == "delete"){

                    $array = explode(",",$totalJurorScore->scoreByJuror1);
                    $count = count($array);
                    $score = $array[$count-1];
                    $array2 = explode("+", $score);
                    $count2 = count($array2); 
                    if($count2 > 1){
                        $total = $array2[0] + $array2[1];
                    }else{
                        $total = $array2[0];
                    }
                    if($side == "red"){
                        $redScore = $redScore->redScore - $total;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['redScore' => $redScore]);
                    }elseif($side == "blue"){
                        $blueScore = $blueScore->blueScore - $total;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['blueScore' => $blueScore]);
                    }

                    $totalScoreJuror1 = $totalJurorScore->scoreByJuror1Value - $total;
                    $totalScore = $totalScore->totalScore - $total;

                    array_pop($array);
                    $count = count($array);
                    $JurorScoreView = null;
                    for ($i=0; $i < $count ; $i++) { 
                        if(!is_null($JurorScoreView)){
                            $JurorScoreView = $JurorScoreView.",".$array[$i];
                        }else{
                            $JurorScoreView = $array[$i];
                        }
                    }
                    

                    DB::table('competition_score')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->update(['scoreByJuror1' => $JurorScoreView, 'scoreByJuror1Value' => $totalScoreJuror1, 'totalScore' => $totalScore ]);
                
                }elseif($status == "reset"){
                    $totalScore = $totalScore->totalScore - $totalJurorScore->scoreByJuror1Value;
                    if($side == "red"){
                        $redScore = $redScore->redScore - $totalJurorScore->scoreByJuror1Value;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['redScore' => $redScore]);
                    }elseif($side == "blue"){
                        $blueScore = $blueScore->blueScore - $totalJurorScore->scoreByJuror1Value;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['blueScore' => $blueScore]);
                    }
                    DB::table('competition_score')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->update(['scoreByJuror1' => null, 'scoreByJuror1Value' => 0, 'totalScore' => $totalScore ]);
                }

                break;
            case 2:
            
                $totalJurorScore = DB::table('competition_score')->select('scoreByJuror2', 'scoreByJuror2Value')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->first();
                $totalJurorScoreValue = 0;
                if($status == "insert"){
                    if(!is_null($totalJurorScore->scoreByJuror2)){
                        $totalJurorScoreView = $totalJurorScore->scoreByJuror2.",".$scoreView;
                        $totalJurorScoreValue = $totalJurorScore->scoreByJuror2Value + $score;
                    }else{
                        $totalJurorScoreView = $scoreView;
                        $totalJurorScoreValue = $score;
                    }
                    DB::table('competition_score')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->update(['scoreByJuror2' => $totalJurorScoreView, 'scoreByJuror2Value' => $totalJurorScoreValue, 'totalScore' => $totalScore ]);
                }elseif($status == "delete"){

                    $array = explode(",",$totalJurorScore->scoreByJuror2);
                    $count = count($array);
                    $score = $array[$count-1];
                    $array2 = explode("+", $score);
                    $count2 = count($array2); 
                    if($count2 > 1){
                        $total = $array2[0] + $array2[1];
                    }else{
                        $total = $array2[0];
                    }
                    if($side == "red"){
                        $redScore = $redScore->redScore - $total;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['redScore' => $redScore]);
                    }elseif($side == "blue"){
                        $blueScore = $blueScore->blueScore - $total;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['blueScore' => $blueScore]);
                    }

                    $totalScoreJuror2 = $totalJurorScore->scoreByJuror2Value - $total;
                    $totalScore = $totalScore->totalScore - $total;

                    array_pop($array);
                    $count = count($array);
                    $JurorScoreView = null;
                    for ($i=0; $i < $count ; $i++) { 
                        if(!is_null($JurorScoreView)){
                            $JurorScoreView = $JurorScoreView.",".$array[$i];
                        }else{
                            $JurorScoreView = $array[$i];
                        }
                    }
                    

                    DB::table('competition_score')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->update(['scoreByJuror3' => $JurorScoreView, 'scoreByJuror2Value' => $totalScoreJuror2, 'totalScore' => $totalScore ]);
                
                }elseif($status == "reset"){
                    $totalScore = $totalScore->totalScore - $totalJurorScore->scoreByJuror2Value;
                    if($side == "red"){
                        $redScore = $redScore->redScore - $totalJurorScore->scoreByJuror2Value;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['redScore' => $redScore]);
                    }elseif($side == "blue"){
                        $blueScore = $blueScore->blueScore - $totalJurorScore->scoreByJuror3Value;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['blueScore' => $blueScore]);
                    }
                    DB::table('competition_score')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->update(['scoreByJuror2' => null, 'scoreByJuror2Value' => 0, 'totalScore' => $totalScore ]);
                }
                break;

            case 3:
                $totalJurorScore = DB::table('competition_score')->select('scoreByJuror3', 'scoreByJuror3Value')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->first();
                $totalJurorScoreValue = 0;

                if($status == "insert"){
                    if(!is_null($totalJurorScore->scoreByJuror3)){
                        $totalJurorScoreView = $totalJurorScore->scoreByJuror3.",".$scoreView;
                        $totalJurorScoreValue = $totalJurorScore->scoreByJuror3Value + $score;
                    }else{
                        $totalJurorScoreView = $scoreView;
                        $totalJurorScoreValue = $score;
                    }
                    DB::table('competition_score')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->update(['scoreByJuror3' => $totalJurorScoreView, 'scoreByJuror3Value' => $totalJurorScoreValue, 'totalScore' => $totalScore ]);
                }elseif($status == "delete"){

                    $array = explode(",",$totalJurorScore->scoreByJuror3);
                    $count = count($array);
                    $score = $array[$count-1];
                    $array2 = explode("+", $score);
                    $count2 = count($array2); 
                    if($count2 > 1){
                        $total = $array2[0] + $array2[1];
                    }else{
                        $total = $array2[0];
                    }
                    if($side == "red"){
                        $redScore = $redScore->redScore - $total;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['redScore' => $redScore]);
                    }elseif($side == "blue"){
                        $blueScore = $blueScore->blueScore - $total;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['blueScore' => $blueScore]);
                    }

                    $totalScoreJuror3 = $totalJurorScore->scoreByJuror3Value - $total;
                    $totalScore = $totalScore->totalScore - $total;

                    array_pop($array);
                    $count = count($array);
                    $JurorScoreView = null;
                    for ($i=0; $i < $count ; $i++) { 
                        if(!is_null($JurorScoreView)){
                            $JurorScoreView = $JurorScoreView.",".$array[$i];
                        }else{
                            $JurorScoreView = $array[$i];
                        }
                    }
                    

                    DB::table('competition_score')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->update(['scoreByJuror3' => $JurorScoreView, 'scoreByJuror3Value' => $totalScoreJuror3, 'totalScore' => $totalScore ]);
                
                }elseif($status == "reset"){
                    $totalScore = $totalScore->totalScore - $totalJurorScore->scoreByJuror3Value;
                    if($side == "red"){
                        $redScore = $redScore->redScore - $totalJurorScore->scoreByJuror3Value;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['redScore' => $redScore]);
                    }elseif($side == "blue"){
                        $blueScore = $blueScore->blueScore - $totalJurorScore->scoreByJuror3Value;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['blueScore' => $blueScore]);
                    }
                    DB::table('competition_score')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->update(['scoreByJuror3' => null, 'scoreByJuror3Value' => 0, 'totalScore' => $totalScore ]);
                }
                break;

            case 4:
                $totalJurorScore = DB::table('competition_score')->select('scoreByJuror4', 'scoreByJuror4Value')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->first();
                $totalJurorScoreValue = 0;
                if($status == "insert"){
                    if(!is_null($totalJurorScore->scoreByJuror4)){
                        $totalJurorScoreView = $totalJurorScore->scoreByJuror4.",".$scoreView;
                        $totalJurorScoreValue = $totalJurorScore->scoreByJuror4Value + $score;
                    }else{
                        $totalJurorScoreView = $scoreView;
                        $totalJurorScoreValue = $score;
                    }
                    DB::table('competition_score')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->update(['scoreByJuror4' => $totalJurorScoreView, 'scoreByJuror4Value' => $totalJurorScoreValue, 'totalScore' => $totalScore ]);
                }elseif($status == "delete"){

                    $array = explode(",",$totalJurorScore->scoreByJuror4);
                    $count = count($array);
                    $score = $array[$count-1];
                    $array2 = explode("+", $score);
                    $count2 = count($array2); 
                    if($count2 > 1){
                        $total = $array2[0] + $array2[1];
                    }else{
                        $total = $array2[0];
                    }
                    if($side == "red"){
                        $redScore = $redScore->redScore - $total;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['redScore' => $redScore]);
                    }elseif($side == "blue"){
                        $blueScore = $blueScore->blueScore - $total;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['blueScore' => $blueScore]);
                    }

                    $totalScoreJuror4 = $totalJurorScore->scoreByJuror4Value - $total;
                    $totalScore = $totalScore->totalScore - $total;

                    array_pop($array);
                    $count = count($array);
                    $JurorScoreView = null;
                    for ($i=0; $i < $count ; $i++) { 
                        if(!is_null($JurorScoreView)){
                            $JurorScoreView = $JurorScoreView.",".$array[$i];
                        }else{
                            $JurorScoreView = $array[$i];
                        }
                    }
                    

                    DB::table('competition_score')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->update(['scoreByJuror4' => $JurorScoreView, 'scoreByJuror4Value' => $totalScoreJuror4, 'totalScore' => $totalScore ]);
                
                }elseif($status == "reset"){
                    $totalScore = $totalScore->totalScore - $totalJurorScore->scoreByJuror4Value;
                    if($side == "red"){
                        $redScore = $redScore->redScore - $totalJurorScore->scoreByJuror4Value;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['redScore' => $redScore]);
                    }elseif($side == "blue"){
                        $blueScore = $blueScore->blueScore - $totalJurorScore->scoreByJuror4Value;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['blueScore' => $blueScore]);
                    }
                    DB::table('competition_score')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->update(['scoreByJuror4' => null, 'scoreByJuror4Value' => 0, 'totalScore' => $totalScore ]);
                }
                break;

            case 5:
                $totalJurorScore = DB::table('competition_score')->select('scoreByJuror5', 'scoreByJuror5Value')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->first();
                $totalJurorScoreValue = 0;
                if($status == "insert"){
                    if(!is_null($totalJurorScore->scoreByJuror5)){
                        $totalJurorScoreView = $totalJurorScore->scoreByJuror5.",".$scoreView;
                        $totalJurorScoreValue = $totalJurorScore->scoreByJuror5Value + $score;
                    }else{
                        $totalJurorScoreView = $scoreView;
                        $totalJurorScoreValue = $score;
                    }
                    DB::table('competition_score')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->update(['scoreByJuror5' => $totalJurorScoreView, 'scoreByJuror5Value' => $totalJurorScoreValue, 'totalScore' => $totalScore ]);
                }elseif($status == "delete"){

                    $array = explode(",",$totalJurorScore->scoreByJuror5);
                    $count = count($array);
                    $score = $array[$count-1];
                    $array2 = explode("+", $score);
                    $count2 = count($array2); 
                    if($count2 > 1){
                        $total = $array2[0] + $array2[1];
                    }else{
                        $total = $array2[0];
                    }
                    if($side == "red"){
                        $redScore = $redScore->redScore - $total;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['redScore' => $redScore]);
                    }elseif($side == "blue"){
                        $blueScore = $blueScore->blueScore - $total;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['blueScore' => $blueScore]);
                    }

                    $totalScoreJuror5 = $totalJurorScore->scoreByJuror5Value - $total;
                    $totalScore = $totalScore->totalScore - $total;

                    array_pop($array);
                    $count = count($array);
                    $JurorScoreView = null;
                    for ($i=0; $i < $count ; $i++) { 
                        if(!is_null($JurorScoreView)){
                            $JurorScoreView = $JurorScoreView.",".$array[$i];
                        }else{
                            $JurorScoreView = $array[$i];
                        }
                    }
                    

                    DB::table('competition_score')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->update(['scoreByJuror5' => $JurorScoreView, 'scoreByJuror5Value' => $totalScoreJuror5, 'totalScore' => $totalScore ]);
                
                }elseif($status == "reset"){
                    $totalScore = $totalScore->totalScore - $totalJurorScore->scoreByJuror5Value;
                    if($side == "red"){
                        $redScore = $redScore->redScore - $totalJurorScore->scoreByJuror5Value;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['redScore' => $redScore]);
                    }elseif($side == "blue"){
                        $blueScore = $blueScore->blueScore - $totalJurorScore->scoreByJuror5Value;
                        DB::table('detail_competition')->where('detailCompetitionId', $detailCompetitionId->detailCompetitionId)->where('round', $round)->update(['blueScore' => $blueScore]);
                    }
                    DB::table('competition_score')->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->where('competitionScoreId', $competitionScoreId[$round-1]->competitionScoreId)->update(['scoreByJuror5' => null, 'scoreByJuror5Value' => 0, 'totalScore' => $totalScore ]);
                }
                break;    
        }

        return response()->json([
            'status' => 'success',
            'jurorNumber' => $jurorNumber->jurorNumber,
            'competitionId' => $competitionId,
            'side' => $side,
            'competitionScoreId' => $competitionScoreId[$round-1]->competitionScoreId,
            // 'scoreByJuror' => $totalJurorScore,
            'totalScore' => $totalScore,
            ]);

    }


    /*
        status : start
        conpetitionId
        round
        */
    public function monitoringCompetition(Request $request){
        $value = $request->json()->all();
        if($value['round'] == 0){
            return response()->json(['status' => 'fail', 'message' => "please select round first"]);
        }
        switch ($value['status']) {
            case "start":
                $check = DB::table('detail_competition')->where('competitionId', $value['competitionId'])->where('runCompetition', 1)->first();
                if(!is_null($check)){
                    return response()->json(['status' => 'fail', 'round' => $value['round'], 'message' => "round ".$check->round." is ongoing"]);
                }
                $last = DB::table('detail_competition')->select('detailCompetitionId')->orderBy('detailCompetitionId', 'DSC')->first();
                
                $detailCompetition = new DetailCompetition;
                $detailCompetition->competitionId = $value['competitionId'];
                $detailCompetition->detailCompetitionId = $last->detailCompetitionId + 1;
                $detailCompetition->round = $value['round'];
                $detailCompetition->runCompetition = 1;
                // print_r($detailCompetition->detailCompetitionId);
                
                // print_r($detailCompetition->detailCompetitionId);
                $participant = DB::table('competition')->select('redCorner', 'blueCorner')->where('competitionId', $value['competitionId'])->first();
                // print_r($participant);
                for ($i=0; $i < 2 ; $i++) {

                    $last = DB::table('competition_score')->select('competitionScoreId')->orderBy('competitionScoreId', 'DSC')->first();
                    $insert = array([
                    'competitionScoreId' => $last->competitionScoreId + 1,
                    'detailCompetitionId' => $detailCompetition->detailCompetitionId,
                    'scoreByJuror1Value' => 0,
                    'scoreByJuror2Value' => 0,
                    'scoreByJuror3Value' => 0,
                    'scoreByJuror4Value' => 0,
                    'scoreByJuror5Value' => 0,
                    'scoreByJuror1' => 0,
                    'scoreByJuror2' => 0,
                    'scoreByJuror3' => 0,
                    'scoreByJuror4' => 0,
                    'scoreByJuror5' => 0,
                    'totalScore' => 0
                    ]);
                    DB::table('competition_score')->insert($insert);
                
                    // print_r($participant);
                        $check = DB::table('participant_score')->where('participantId', $participant->redCorner)->where('competitionId', $last->competitionScoreId )->first();
                        // print_r($check);
                        if ($i == 0){
                        $insert = array([
                            'participantId' => $participant->redCorner,
                            'competitionId' => $value['competitionId'],
                            'competitionScoreId' => $last->competitionScoreId + 1
                        ]);
                        DB::table('participant_score')->insert($insert);
                        }else{
                            $insert = array([
                                'participantId' => $participant->blueCorner,
                                'competitionId' => $value['competitionId'],
                                'competitionScoreId' => $last->competitionScoreId + 1
                            ]);
                            DB::table('participant_score')->insert($insert);
                        }
                }
                $detailCompetition->save();
            
                return response()->json(['status' => 'success', 'round' => $value['round'], 'competitionStatus' => $value['status']]);
                break;

            case "stop":
                $id = DB::table('detail_competition')->select('detailCompetitionId')->where('competitionId', $value['competitionId'])->where('round', $value['round'])->where('runCompetition', 1)->first();
                if(!is_null($id)){
                    DB::table('detail_competition')->where('detailCompetitionId', $id->detailCompetitionId)->update(['runCompetition' => 0]);
                    
                    return response()->json(['status' => 'success', 'round' => $value['round'], 'competitionStatus' => $value['status']]);
                }else{
                    return response()->json(['status' => 'fail', 'round' => $value['round'], 'message' => "round hasn't started yet"]);
                }
                break;

            case "finish":
                
                if ($value['winStatus'] != "score"){
                    $win = $this->monitoringByWinning($request);
                }else{
                    if($value['round'] != 3){
                        return response()->json([
                            'status' => 'fail',
                            'round' => $value['round'],
                            'message' => "competition isn't finish in round 3"
                        ]);
                    }else{
                        $competitionScore = DB::table('detail_competition')->where('competitionId', $value['competitionId'])->get();
                        if(is_null($competitionScore)){
                            $win = 1;
                        }else{
                            $win = 0;
                            $redScore = 0;
                            $blueScore = 0;
                            $redPoint = 0;
                            $bluePoint = 0;
                            for ($i=0; $i < $value['round'] ; $i++) { 
                                $redScore = $competitionScore[$i]->redScore + $redScore;
                                $blueScore = $competitionScore[$i]->blueScore + $blueScore;
                                if($redScore < $blueScore){
                                    $bluePoint = $bluePoint + 1;
                                }elseif($redScore > $blueScore){
                                    $redPoint = $redPoint + 1;
                                }
                            }
                            $competition = Competition::findOrFail($value['competitionId']);
                            if($redPoint < $bluePoint){
                                $winner = DB::table('competition')->select('blueCorner')->where('competitionId', $value['competitionId'])->first();
                                $competition->winner = $winner->blueCorner;
                                $value['winner'] = "BLUE";
                            }elseif($redPoint > $bluePoint){
                                $winner = DB::table('competition')->select('redCorner')->where('competitionId', $value['competitionId'])->first();
                                $competition->winner = $winner->redCorner;
                                $value['winner'] = "RED";
                            }elseif($redPoint == $bluePoint){
                                return response()->json([
                                    'status' => "success",
                                    'finalScore' => $competition->finalScore,
                                    'winStatus' => "DRAW",
                                    // 'nextStage' => $nextStage->competitionId
                                ]);
                            }
                            $competition->finalScore = $redPoint."-".$bluePoint;
                            $competition->save();
                            $id = DB::table('detail_competition')->select('detailCompetitionId')->where('competitionId', $value['competitionId'])->where('round', $value['round'])->where('runCompetition', 0)->first();
                            DB::table('detail_competition')->where('detailCompetitionId', $id->detailCompetitionId)->update(['runCompetition' => 0, 'competitionStatus' => "FINISH", 'winStatus' => $value['winStatus']]);
                        }
                    }
                }
                    
                if ($win == 1){
                    return response()->json([
                        'status' => 'fail',
                        'round' => $value['round'],
                        'message' => "round not found"
                    ]);
                }else{
                    $competition = DB::table('competition')->where('competitionId', $value['competitionId'])->first();
                    $nextStage = Competition::where('classId', $competition->classId)->where('bracketNo', $competition->nextStage)->first();
                    if($competition->bracketNo == $nextStage->noWinRed){
                        $nextStage->redCorner = $competition->winner;
                        $nextStage->save();
                    }elseif($competition->bracketNo == $nextStage->noWinBlue){
                        $nextStage->blueCorner = $competition->winner;
                        $nextStage->save();
                    }
                    return response()->json([
                        'status' => "success",
                        'winStatus' => $value['winStatus'],
                        'winner' => $value['winner'],
                        'finalScore' => $competition->finalScore,
                        'nextStage' => $nextStage->competitionId
                    ]);
                }
            
                break;

        }
    }

    public function monitoringByWinning(Request $request){

        $value = $request->json()->all();
        
        if ($value['winStatus'] == "K.O" || $value['winStatus'] == "W.M.P"){
            $id = DB::table('detail_competition')->select('detailCompetitionId')->where('competitionId', $value['competitionId'])->where('round', $value['round'])->where('runCompetition', 0)->first();
            if(is_null($id)){
                return 1;
            }else{
                DB::table('detail_competition')->where('detailCompetitionId', $id->detailCompetitionId)->update(['runCompetition' => 0, 'competitionStatus' => "FINISH", 'winStatus' => $value['winStatus']]);
                $competition = Competition::findOrFail($value['competitionId']);
                if($value['winner'] == "RED"){
                    $competition->winner = $competition->redCorner;
                }else{
                    $competition->winner = $competition->blueCorner;
                }
                
                $competition->save();
            }
        }elseif($value['winStatus'] == "W.O" || $value['winStatus'] == "DIS"){
            $last = DB::table('detail_competition')->select('detailCompetitionId')->orderBy('detailCompetitionId', 'DSC')->first();

            $detailCompetition = new DetailCompetition;
            $detailCompetition->competitionId = $value['competitionId'];
            $detailCompetition->detailCompetitionId = $last->detailCompetitionId + 1;
            $detailCompetition->round = $value['round'];
            $detailCompetition->runCompetition = 0;
            $detailCompetition->competitionStatus = "FINISH";
            $detailCompetition->winStatus = $value['winStatus'];
            $detailCompetition->save();
            $competition = Competition::findOrFail($value['competitionId']);
            if($value['winner'] == "RED"){
                $competition->winner = $competition->redCorner;
            }else{
                $competition->winner = $competition->blueCorner;
            }
            $competition->save();
            return 0;

        }
    }

    public function  startCompetition(Request $request){
        
        $value = $request->json()->all();
        if ($value['status'] == "start"){

            
                # code...
            $last = DB::table('detail_competition')->select('detailCompetitionId')->orderBy('detailCompetitionId', 'DSC')->first();
            
            $detailCompetition = new DetailCompetition;
            $detailCompetition->competitionId = $value['competitionId'];
            $detailCompetition->detailCompetitionId = $last->detailCompetitionId + 1;
            $detailCompetition->round = $value['round'];
            $detailCompetition->runCompetition = 1;
            // print_r($detailCompetition->detailCompetitionId);
            
            print_r($detailCompetition->detailCompetitionId);
            $participant = DB::table('competition')->select('redCorner', 'blueCorner')->where('competitionId', $value['competitionId'])->first();
            print_r($participant);
            for ($i=0; $i < 2 ; $i++) {

                $last = DB::table('competition_score')->select('competitionScoreId')->orderBy('competitionScoreId', 'DSC')->first();
                $insert = array([
                'competitionScoreId' => $last->competitionScoreId + 1,
                'detailCompetitionId' => $detailCompetition->detailCompetitionId,
                ]);
                DB::table('competition_score')->insert($insert);
            
                // print_r($participant);
                    $check = DB::table('participant_score')->where('participantId', $participant->redCorner)->where('competitionId', $last->competitionScoreId )->first();
                    // print_r($check);
                    if ($i == 0){
                    $insert = array([
                        'participantId' => $participant->redCorner,
                        'competitionId' => $value['competitionId'],
                        'competitionScoreId' => $last->competitionScoreId + 1
                    ]);
                    DB::table('participant_score')->insert($insert);
                    }else{
                        $insert = array([
                            'participantId' => $participant->blueCorner,
                            'competitionId' => $value['competitionId'],
                            'competitionScoreId' => $last->competitionScoreId + 1
                        ]);
                        DB::table('participant_score')->insert($insert);
                    }
           
        }
            $detailCompetition->save();
        
        
            return response()->json(['status' => 'success', 'round' => $value['round'], 'competitionStatus' => $value['status']]);
        }else{
            return response()->json(['status' => 'fail status not start', 'competitionStatus' => $value['status'] ]);
        }

    }

    public function stopCompetition(Request $request){
        $value = $request->json()->all();
        if ($value['status'] == "stop"){
            $id = DB::table('detail_competition')->select('detailCompetitionId')->where('competitionId', $value['competitionId'])->where('round', $value['round'])->where('runCompetition', 1)->first();
            // print_r($detailCompetitionId);
            DB::table('detail_competition')->where('detailCompetitionId', $id->detailCompetitionId)->update(['runCompetition' => 0]);
            
            return response()->json(['status' => 'success', 'round' => $value['round'], 'competitionStatus' => $value['status']]);
        }else{
            return response()->json(['status' => 'fail status not stop', 'competitionStatus' => $value['status'] ]);
        }

    }

    public function finishCompetition(Request $request){
        $value = $request->json()->all();
        if ($value['status'] == "finish"){
            $id = DB::table('detail_competition')->select('detailCompetitionId')->where('competitionId', $value['competitionId'])->where('round', $value['round'])->where('runCompetition', 1)->first();
            DB::table('detail_competition')->where('detailCompetitionId', $id->detailCompetitionId)->update(['runCompetition' => 0, 'competitionStatus' => "FINISH", 'winStatus' => "POINT"]);

            $competitionScore = DB::table('detail_competition')->where('competitionId', $value['competitionId'])->get();
            
            $redScore = 0;
            $blueScore = 0;
            $redPoint = 0;
            $bluePoint = 0;
            for ($i=0; $i < $value['round'] ; $i++) { 
                $redScore = $competitionScore[$i]->redScore + $redScore;
                $blueScore = $competitionScore[$i]->blueScore + $blueScore;
                if($redScore < $blueScore){
                    $bluePoint = $bluePoint + 1;
                }else{
                    $redPoint = $redPoint + 1;
                }
            }
            $competition = Competition::findOrFail($value['competitionId']);
            if($redPoint < $bluePoint){
                $winner = DB::table('competition')->select('blueCorner')->where('competitionId', $value['competitionId'])->first();
                $competition->winner = $winner->blueCorner;
            }else{
                $winner = DB::table('competition')->select('redCorner')->where('competitionId', $value['competitionId'])->first();
                
                $competition->winner = $winner->redCorner;
            }
            $competition->finalScore = $redPoint."-".$bluePoint;
            $competition->save();
            $nextStage = Competition::where('classId', $competition->classId)->where('bracketNo', $competition->nextStage)->first();
            if($competition->bracketNo == $nextStage->noWinRed){
                $nextStage->redCorner = $competition->winner;
                $nextStage->save();
            }elseif($competition->bracketNo == $nextStage->noWinBlue){
                $nextStage->blueCorner = $competition->winner;
                $nextStage->save();
            }
        
            
            return response()->json([
                'status' => "success",
                'winner' => $winner,
                'finalScore' => $competition->finalScore,
                'winStatus' => "POINT",
                'nextStage' => $nextStage->competitionId
            ]);
            
        }

       

    }

    public function showScore(Request $request){

        $competitionId = $request->input('competition');
        $competition = DB::table('competition')->select('redCorner', 'blueCorner', 'winner', 'classId', 'championshipId','qualification')->where('competitionId', $competitionId)->first();
        $detailCompetition = DB::table('detail_competition')->select('detailCompetitionId', 'round')->where('competitionId', $competitionId)->get();
        $redCorner = DB::table('participant')->select('participant.participantName', 'contingent.contingentName','participant.weight')->leftjoin('contingent','contingent.contingentId', '=', 'participant.contingentId')->where('participantId', $competition->redCorner)->first();
        $blueCorner = DB::table('participant')->select('participant.participantName', 'contingent.contingentName','participant.weight')->leftjoin('contingent','contingent.contingentId', '=', 'participant.contingentId')->where('participantId', $competition->blueCorner)->first();
        $class = DB::table('class')->select('className', 'gender')->where('classId', $competition->classId)->first();
        $championship = DB::table('championship')->where('championshipId', $competition->championshipId)->first();
        // print_r($detailCompetition);
        $countDetail = count($detailCompetition);
        $totalAllredJuror1 = 0;
        $totalAllredJuror2 = 0;
        $totalAllredJuror3 = 0;
        $totalAllredJuror4 = 0;
        $totalAllredJuror5 = 0;
        $totalAllblueJuror1 = 0;
        $totalAllblueJuror2 = 0;
        $totalAllblueJuror3 = 0;
        $totalAllblueJuror4 = 0;
        $totalAllblueJuror5 = 0;
        
        $response = [];
        $score = [];
        $score = [];
        $score['round'] = $competition->qualification;
        $score['competitionId'] = $competitionId;
        $score['championshipName'] = $championship->championshipName;
        $score['championshipDateStart'] = $championship->dateStart;
        $score['championshipDateEnd'] = $championship->dateEnd;
        $score['championshipLocation'] = $championship->location;
        $score['competitionId'] = $competitionId;
        $score['redCornerName'] = $redCorner->participantName;
        $score['redCornerContingent'] = $redCorner->contingentName;
        $score['redCornerWeight'] = $redCorner->weight;
        $score['blueCornerName'] = $blueCorner->participantName;
        $score['blueCornerContingent'] = $blueCorner->contingentName;
        $score['blueCornerWeight'] = $blueCorner->weight;
        $score['className'] = $class->className;
        $score['gender'] = $class->gender;
        $score['winner'] = [];
        $score['winner']['winnerName'] = (!is_null($competition->winner)) ? (($competition->winner == $competition->redCorner) ? $score['redCornerName'] :$score['blueCornerName'] ) : null;
        $score['winner']['winnerSide'] = (!is_null($competition->winner)) ? (($competition->winner == $competition->redCorner) ? "red" : "blue" ) : null;
        $response[] = $score;
        $score = [];
        for ($i=0; $i < 3 ; $i++) { 

            $score['round'] = $i+1;
            $score['score'] = [];

            $score['score']['redJuror1'] =  0;
            $score['score']['totalRedJuror1'] =  0;
            $score['score']['redJuror2'] = 0;
            $score['score']['totalRedJuror2'] = 0;
            $score['score']['redJuror3'] = 0;
            $score['score']['totalRedJuror3'] = 0;
            $score['score']['redJuror4'] =  0;
            $score['score']['totalRedJuror4'] = 0;
            $score['score']['redJuror5'] = 0;
            $score['score']['totalRedJuror5'] =  0;
            $score['score']['totalScoreRed'] = 0;
            
            $score['score']['blueJuror1'] = 0;
            $score['score']['totalBlueJuror1'] = 0;
            $score['score']['blueJuror2'] = 0;
            $score['score']['totalBlueJuror2'] = 0;
            $score['score']['blueJuror3'] = 0;
            $score['score']['totalBlueJuror3'] = 0;
            $score['score']['blueJuror4'] = 0;
            $score['score']['totalBlueJuror4'] = 0;
            $score['score']['blueJuror5'] = 0;
            $score['score']['totalBlueJuror5'] = 0;

            $response[] = $score;

        }
        
        
        for ($i=0; $i < $countDetail ; $i++) { 

            
            $check = DB::table('competition_score')->where('detailCompetitionId', $detailCompetition[$i]->detailCompetitionId)->get();
            
            $response[$i+1]['score']['redJuror1'] = (!is_null($check[0]->scoreByJuror1)) ? $check[0]->scoreByJuror1 : 0;
            $response[$i+1]['score']['totalRedJuror1'] = (!is_null($check[0]->scoreByJuror1Value)) ? $check[0]->scoreByJuror1Value : 0;
            $response[$i+1]['score']['redJuror2'] = (!is_null($check[0]->scoreByJuror2)) ? $check[0]->scoreByJuror2 : 0;
            $response[$i+1]['score']['totalRedJuror2'] = (!is_null($check[0]->scoreByJuror2Value)) ? $check[0]->scoreByJuror2Value : 0;
            $response[$i+1]['score']['redJuror3'] = (!is_null($check[0]->scoreByJuror3)) ? $check[0]->scoreByJuror3 : 0;
            $response[$i+1]['score']['totalRedJuror3'] = (!is_null($check[0]->scoreByJuror3Value)) ? $check[0]->scoreByJuror3Value : 0;
            $response[$i+1]['score']['redJuror4'] = (!is_null($check[0]->scoreByJuror4)) ? $check[0]->scoreByJuror4 : 0;
            $response[$i+1]['score']['totalRedJuror4'] = (!is_null($check[0]->scoreByJuror4Value)) ? $check[0]->scoreByJuror4Value : 0;
            $response[$i+1]['score']['redJuror5'] = (!is_null($check[0]->scoreByJuror5)) ? $check[0]->scoreByJuror5 : 0;
            $response[$i+1]['score']['totalRedJuror5'] = (!is_null($check[0]->scoreByJuror5Value)) ? $check[0]->scoreByJuror5Value : 0;
            $response[$i+1]['score']['totalScoreRed'] = $check[0]->totalScore;
            $totalAllredJuror1 = $totalAllredJuror1 + $response[$i+1]['score']['totalRedJuror1'];
            $totalAllredJuror2 = $totalAllredJuror2 + $response[$i+1]['score']['totalRedJuror2'];
            $totalAllredJuror3 = $totalAllredJuror3 + $response[$i+1]['score']['totalRedJuror3'];
            $totalAllredJuror4 = $totalAllredJuror4 + $response[$i+1]['score']['totalRedJuror4'];
            $totalAllredJuror5 = $totalAllredJuror5 + $response[$i+1]['score']['totalRedJuror5'];
            
            $response[$i+1]['score']['blueJuror1'] = (!is_null($check[1]->scoreByJuror1)) ? $check[1]->scoreByJuror1 : 0;
            $response[$i+1]['score']['totalBlueJuror1'] = (!is_null($check[1]->scoreByJuror1Value)) ? $check[1]->scoreByJuror1Value : 0;
            $response[$i+1]['score']['blueJuror2'] = (!is_null($check[1]->scoreByJuror2)) ? $check[1]->scoreByJuror2 : 0;
            $response[$i+1]['score']['totalBlueJuror2'] = (!is_null($check[1]->scoreByJuror2Value)) ? $check[1]->scoreByJuror2Value : 0;
            $response[$i+1]['score']['blueJuror3'] = (!is_null($check[1]->scoreByJuror3)) ? $check[1]->scoreByJuror3 : 0;
            $response[$i+1]['score']['totalBlueJuror3'] = (!is_null($check[1]->scoreByJuror3Value)) ? $check[1]->scoreByJuror3Value : 0;
            $response[$i+1]['score']['blueJuror4'] = (!is_null($check[1]->scoreByJuror4)) ? $check[1]->scoreByJuror4 : 0;
            $response[$i+1]['score']['totalBlueJuror4'] = (!is_null($check[1]->scoreByJuror4Value)) ? $check[1]->scoreByJuror4Value : 0;
            $response[$i+1]['score']['blueJuror5'] = (!is_null($check[1]->scoreByJuror5)) ? $check[1]->scoreByJuror5 : 0;
            $response[$i+1]['score']['totalBlueJuror5'] = (!is_null($check[1]->scoreByJuror5Value)) ? $check[1]->scoreByJuror5Value : 0;
            $response[$i+1]['score']['totalScoreBlue'] = $check[1]->totalScore;

            $totalAllblueJuror1 = $totalAllblueJuror1 + $response[$i+1]['score']['totalBlueJuror1'];
            $totalAllblueJuror2 = $totalAllblueJuror2 + $response[$i+1]['score']['totalBlueJuror2'];
            $totalAllblueJuror3 = $totalAllblueJuror3 + $response[$i+1]['score']['totalBlueJuror3'];
            $totalAllblueJuror4 = $totalAllblueJuror4 + $response[$i+1]['score']['totalBlueJuror4'];
            $totalAllblueJuror5 = $totalAllblueJuror5 + $response[$i+1]['score']['totalBlueJuror5'];
        }
        $score = [];
        $score['totalAllRoundRedJuror1'] = $totalAllredJuror1;
        $score['totalAllRoundRedJuror2'] = $totalAllredJuror2;
        $score['totalAllRoundRedJuror3'] = $totalAllredJuror3;
        $score['totalAllRoundRedJuror4'] = $totalAllredJuror4;
        $score['totalAllRoundRedJuror5'] = $totalAllredJuror5;

        $score['totalAllRoundBlueJuror1'] = $totalAllblueJuror1;
        $score['totalAllRoundBlueJuror2'] = $totalAllblueJuror2;
        $score['totalAllRoundBlueJuror3'] = $totalAllblueJuror3;
        $score['totalAllRoundBlueJuror4'] = $totalAllblueJuror4;
        $score['totalAllRoundBlueJuror5'] = $totalAllblueJuror5;

        

        
        $response[] = $score;

        return response()->json($response);
    }
    

    

}

<?php

namespace App\Http\Controllers;

use App\Official;
use Illuminate\Http\Request;

class OfficialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $official = new Official;

        $official->officialName = $request->input('name');
        $official->officialId = $request->input('id');
        $official->contingentId = $request->input('contingentId');
        $official->username = $request->input('username');
        $official->password = $request->input('password');
        
        if ($official->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
    }

    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function showById($id)
    {
        // $official = new Official;
        // $official = Official::find(1);
        // // echo($id);
        // // var_dump($official);
        // return response()->json($result);
        return response()->json(Official::findOrFail($id));
    }
    
    /**
     * Retrieve the all user 
     *
     * @return Response
     */
    public function showAllOfficial()
    {
        return response()->json(Official::all());
    }

    /**
     * Update the user for the given ID.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $official = Official::findOrFail($id);
        echo($official);
        $official->officialName = $request->input('officialName');
        $official->competitionId = $request->input('competitionId');
        $official->username = $request->input('username');
        $official->password = $request->input('password');
        
        if ($official->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
        

    }

    /**
     * Delete the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        Official::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    public function authenticate(Request $request)
 
   {
 
       $this->validate($request, [
 
       'username' => 'required',
 
       'password' => 'required'
 
        ]);
 
      $operator = Official::where('username', $request->input('username'))->first();

      if ($request->input('password') == $operator->password){
 
    //  if(Hash::check($request->input('password'), $operator->password)){
    
 
          $apikey = base64_encode(str_random(40));
 
          Operator::where('username', $request->input('username'))->update(['api_key' => "$apikey"]);;
 
          return response()->json(['status' => 'success','api_key' => $apikey]);
 
      }else{
 
          return response()->json(['status' => 'fail'],401);
 
      }
 
   }
}

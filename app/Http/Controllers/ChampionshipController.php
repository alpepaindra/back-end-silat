<?php

namespace App\Http\Controllers;

use App\Championship;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ChampionshipController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $last = DB::table('championship')->select('championshipId')->orderBy('championShipId', 'DSC')->first();
        $championship = new Championship;

        $championship->championshipName = $request->json()->get('championshipName');
        $championship->championshipId = $last->championshipId + 1;
        $championship->dateStart = $request->json()->get('dateStart');
        $championship->dateEnd = $request->json()->get('dateEnd');
        $championship->description = $request->json()->get('description');
        $championship->location = $request->json()->get('location');

        
        // if ($request->file('photo')->isValid())
        // {
        //     $championship->championshipLogo = $request->file('championshipLogo');
        // }else{
        //     return getErrorMessage();
        // }
        
        if($championship->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
    }

    public function showChampionshipBy(Request $request){
        $name = $request->input('name');
        $dateStart = $request->input('dateStart');
        $dateEnd = $request->input('dateEnd');
        $location = $request->input('location');
        $data =  DB::table('championship');

        if($name != null){
            $data = $data->where('championshipName', 'like', "%".$name."%");
        }
        if($dateStart !=0){
            $data = $data->where('dateStart', $dateStart);
        }
        if($dateEnd !=0){
            $data = $data->where('dateEnd', $dateEnd);
        }
        if($location != null){
            $data = $data->where('location', 'like', "%".$location."%");
        }
        $data = $data->get();
        
        return response()->json($data);

    }

    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function showById($id)
    {
        // $championship = new Championship;
        // $championship = Championship::find(1);
        // // echo($id);
        // // var_dump($championship);
        // return response()->json($result);
        return response()->json(Championship::findOrFail($id));
    }
    
    /**
     * Retrieve the all user 
     *
     * @return Response
     */
    public function showAllChampionship()
    {
            return response()->json(Championship::all());
        
    }

    /**
     * Update the user for the given ID.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $championship = Championship::findOrFail($id);
        $championship->championshipName = $request->json()->get('championshipName');
        $championship->dateStart = $request->json()->get('dateStart');
        $championship->dateEnd = $request->json()->get('dateEnd');
        $championship->description = $request->json()->get('description');
        $championship->location = $request->json()->get('location');
        // if ($request->file('photo')->isValid())
        // {
        //     $championship->championshipLogo = $request->file('championshipLogo');
        // }else{
        //     return getErrorMessage();
        // }
        
        if($championship->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
        

    }



    /**
     * Delete the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        Championship::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}

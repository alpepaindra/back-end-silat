<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $category = new Category;

        $category->categoryName = $request->input('name');
        $category->categoryId = $request->input('id');
        $category->competitionId = $request->input('competitionId');
        var_dump($request->input('name'));
        
        if($category->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
    }

    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function showById($id)
    {
        // $category = new Category;
        // $category = Category::find(1);
        // // echo($id);
        // // var_dump($category);
        // return response()->json($result);
        return response()->json(Category::findOrFail($id));
    }
    
    /**
     * Retrieve the all user 
     *
     * @return Response
     */
     
    public function showAllCategory()
    {
        return response()->json(Category::all());
    }

    /**
     * Update the user for the given ID.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $category = Category::findOrFail($id);
        $category->categoryName = $request->input('categoryName');
        $category->competitionId = $request->input('competitionId');
        
        if($category->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }

    }

    /**
     * Delete the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        Category::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}

<?php

namespace App\Http\Controllers;

use App\Statistic;
use App\Helper\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticController extends Controller
{
    
    
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $statistic = new Statistic;

        $statistic->kickAmount = $request->input('kickAmount');
        $statistic->kickOnTarget = $request->input('kickOnTarget');
        $statistic->kickMisses = $request->input('kickMisses');
        $statistic->punchAmmount = $request->input('punchAmmount');
        $statistic->punchOnTarget = $request->input('punchOnTarget');
        $statistic->punchMisses = $request->input('punchMisses');
        $statistic->fallenAmmount = $request->input('fallenAmmount');
        $statistic->validFallen = $request->input('validFallen');
        $statistic->failedFallen = $request->input('failedFallen');
        $statistic->repelAmmount = $request->input('repelAmmount');
        $statistic->dodgeAmmount = $request->input('dodgeAmmount');
        $statistic->punishmentAmmount = $request->input('punishmentAmmount');
        

        if ($statistic->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
    }

    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function showById($id)
    {
        // $statistic = new Statistic;
        // $statistic = Statistic::find(1);
        // // echo($id);
        // // var_dump($statistic);
        // return response()->json($result);
        return response()->json(Statistic::findOrFail($id));
    }
    
    /**
     * Retrieve the all user 
     *
     * @return Response
     */
    public function showAllStatistic(Request $request)
    {
        return response()->json(Statistic::all());
    }

    /**
     * Update the user for the given ID.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $statistic = Statistic::findOrFail($id);
        $statistic->kickAmount = $request->input('kickAmount');
        $statistic->kickOnTarget = $request->input('kickOnTarget');
        $statistic->kickMisses = $request->input('kickMisses');
        $statistic->punchAmmount = $request->input('punchAmmount');
        $statistic->punchOnTarget = $request->input('punchOnTarget');
        $statistic->punchMisses = $request->input('punchMisses');
        $statistic->fallenAmmount = $request->input('fallenAmmount');
        $statistic->validFallen = $request->input('validFallen');
        $statistic->failedFallen = $request->input('failedFallen');
        $statistic->repelAmmount = $request->input('repelAmmount');
        $statistic->dodgeAmmount = $request->input('dodgeAmmount');
        $statistic->punishmentAmmount = $request->input('punishmentAmmount');
        

        if ($statistic->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
        
        // return response()->json($statistic, 200);
    }

    /**
     * Delete the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        Statistic::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}

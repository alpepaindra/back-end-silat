<?php

namespace App\Http\Controllers;

use App\Competition;
use App\Http\Controllers\GlobalFunctionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;

class CompetitionController extends Controller
{
    
    
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function generateCompetition(Request $request)
    {
        $cek_data = DB::table('competition')->select('championshipId')
        ->where('championshipId', $request->json()->get('championship'))
        ->where('classId', $request->json()->get('classId'))
        ->get();
        $countParticipant = 0 ;

        if(count($cek_data)>0){
            // return response()->json($cek_data);
            return response()->json([
                'status' => 'fail',
                'msg' => 'Bagan telah ada !',
                // 'data' => $cek_data
             ]);
        }else{
            $global = new GlobalFunctionController;
            $participant = DB::table('participant')->select('participantId')->where('classId', $request->json()->get('classId'))->get();
            $countParticipant = count($participant);
            if($countParticipant <1){
                return response()->json([
                    'status' => 'fail',
                    'msg' => 'Peserta Tidak ada !',
                    // 'data' => $cek_data
                 ]);
            }else{
                // print_r($countParticipant);
                $brackets = $global->generateBracket($countParticipant);

                $random_list = $this->RandomNumbers($countParticipant); // random list participant
                $partic_no = 0;

                
                // print_r($participant);
                $no_pertandingan = 1;
                $scheme = DB::table('competition_scheme')
                ->insertGetId(['championshipId' => $request->json()->get('championship'), 'schemeName' => $request->json()->get('schemeName')]);
                $competitionNumber = 1;
                $response = TRUE;

                if ($brackets){
                    //@transaction
                    DB::beginTransaction();

                    try {
                        foreach ($brackets as &$bracket) {

                            $competition = new Competition;
                            $redCorner = null;
                            $blueCorner = null;
                            if ($bracket['bye']) continue;

                            $competition->schemeId = $scheme;
                            $competition->bracketNo = $bracket['bracketNo'];
                            $competition->noWinRed = $bracket['lastGames'] ? $bracket['lastGames'][0]['bracketNo'] : null;
                            $competition->noWinBlue = $bracket['lastGames'] ? $bracket['lastGames'][1]['bracketNo'] : null;
                            $competition->qualification = $bracket['roundNo'];
                            $competition->nextStage = $bracket['nextGame'];
                            $competition->lastStage = json_encode( $bracket['lastGames'] );
                            $competition->competitionNumber = $competitionNumber;
                            $competition->classId = $request->json()->get('classId');
                            $competition->championshipId = $request->json()->get('championship');

                            $competitionNumber++;

                            $temp = [];
                            $temp['participantName'] = [];
                            if(!is_null($random_list) && $partic_no < $countParticipant){

                                $competition->redCorner = $participant[$random_list[$partic_no]]->participantId;
                                $partic_no++;

                                $redCorner = $competition->redCorner;



                                if (!$bracket['lastGames'][0]['bye']){
                                    $competition->blueCorner = $participant[$random_list[$partic_no]]->participantId;
                                    $partic_no++; 
                                    $blueCorner = $competition->blueCorner;

                                }elseif ($bracket['lastGames'][0]['bye'] && $bracket['lastGames'][1]['bye']){
                                    $competition->blueCorner = $participant[$random_list[$partic_no]]->participantId;
                                    $partic_no++;
                                    $blueCorner = $competition->blueCorner;

                                }else{
                                    $temp['participantName']['blue'] = null;
                                }

                            }else{
                                $temp['participantName']['red'] = null;
                                $temp['participantName']['blue'] = null;
                            }

                            $competition->save();
                            if(!is_null($redCorner)){
                                DB::table('follows')->insert(
                                    ['competitionId' => $competition->competitionId, 'participantId' => $redCorner]);

                                if(!is_null($blueCorner)){
                                    DB::table('follows')->insert(
                                        ['competitionId' => $competition->competitionId, 'participantId' => $blueCorner]);
                                }else{
                                    DB::table('follows')->insert(
                                        ['competitionId' => $competition->competitionId, 'participantId' => 0]);
                                }
                            }


                        }    

                    //@transaction
                        DB::commit();
                    } catch (\Exception $e) {
                        $response = FALSE;
                        DB::rollback();
                        throw $e;
                    } catch (\Throwable $e) {
                        $response = FALSE;
                        DB::rollback();
                        throw $e;
                    }
                    
                }
                if($response)
                {
                    return response()->json([
                        'status' => 'success', 
                        'schemeId'=> $scheme, 
                        'countParticipant' => $countParticipant]);
                }else{
                    return response()->json(['status' => 'fail', 'msg' => 'Bagan gagal digenerate !']);
                }
            }
        }
    }

    
    //add @kaha
    public function showCompetitionSchemeAll(Request $request){
        $data = DB::table('competition_scheme')->select('*')
        // ->where('championshipId', $request->input('championship'))
        ->get();
        return response()->json($data);
    }

    public function RandomNumbers($total) {
        if($total > 0){
            $numbers = range(0, ($total-1));
            shuffle($numbers);
            return array_slice($numbers, 0, $total);
        }else{
            return response()->json(['status' => 'fail']);
        }
        
    }
        

    public function setWinner(Request $request){
    
        $competition = Competition::findOrFail($request->input('competitionId'));
        $competition->winner = $request->input('winner');
        $competition->finalScore = $request->input('finalScore');
        $competition->save();
        $nextCompetition = Competition::where('noWinRed', $competition->bracketNo)->where('classId', $competition->classId)->first();
        if($nextCompetition){
            $nextCompetition->redCorner = $competition->winner;
            $nextCompetition->save();
            $data = array('participantId'=>$nextCompetition->redCorner, 'competitionId'=>$nextCompetition->competitionId);
            if($data){
                DB::table('follows')->insert($data);
            }
        }
        $nextCompetition = Competition::where('noWinBlue', $competition->bracketNo)->where('classId', $competition->classId)->first();
        if($nextCompetition){
            $nextCompetition->blueCorner = $competition->winner;
            $nextCompetition->save();
            $data = array('participantId'=>$nextCompetition->blueCorner, 'competitionId'=>$nextCompetition->competitionId);
            if($data){
                DB::table('follows')->insert($data);
            }
        }

        return($nextCompetition);
    }

    public function setSchedule(Request $request){
        
        date_default_timezone_set("Asia/Bangkok");
        $currentDate =  date("Y-m-d");
        
        
        $check = $request->json()->all();
        
        if($check['date'] == null || $check['timeStart'] == null || $check['timeEnd'] == null || $check['arenaId'] == null
        || $check['grandJurorId'] == null|| $check['jurorId1'] == null|| $check['jurorId2'] == null|| $check['jurorId3'] == null
        || $check['jurorId4'] == null || $check['jurorId5'] == null){
            return response()->json([
                'status' => 'fail',
                'message' => "field can not empty"
            ]);
        }else{
            // print_r($check[0]['competitionId'][1]);
            $count = count($check['competitionId']);
            // print_r($count);
            $last = DB::table('competition')->select('scheduleNumber')
                ->where('date',$check['date'])
                ->where('timeStart', $check['timeStart'])
                ->where('timeEnd',$check['timeEnd'])
                ->where('arenaId', $check['arenaId'])
                ->orderBy('scheduleNumber', 'DSC')
                ->first();
                // var_dump($last);
            
            for ($i=0; $i < $count ; $i++) { 
                $last = DB::table('competition')->select('scheduleNumber')
                ->where('date',$check['date'])
                ->where('timeStart', $check['timeStart'])
                ->where('timeEnd',$check['timeEnd'])
                ->where('arenaId', $check['arenaId'])
                ->orderBy('scheduleNumber', 'DSC')
                ->first();
                
                $number = 1;

                $competition = Competition::findOrFail($check['competitionId'][$i]);
                
                if($competition->scheduleNumber == 0){ 
                    if(is_null($last)){
                        $competition->scheduleNumber = $number;
                    }elseif ($last->scheduleNumber == 20){
                        return response()->json([
                            'status' => 'fail',
                            'toalCompetition' => $last->scheduleNumber,
                            'message' => "schedule is full. cannot entry competition ".$check['competitionId'][$i]
                            ]);
                    }else{
                        $competition->scheduleNumber = $last->scheduleNumber + 1;
                        $last->scheduleNumber = $last->scheduleNumber +1;
                    }
                }
                
                $competition->timeStart = $check['timeStart'];
                $competition->timeEnd = $check['timeEnd'];
                $competition->date = $check['date'];
                $competition->arenaId = $check['arenaId'];
                $competition->grandJurorId = $check['grandJurorId'];
                
                
                $scoring = DB::table('scoring')->where('competitionId', $check['competitionId'][$i])->get();
                $countScoring = count($scoring);
                if($countScoring > 0){
                    DB::table('scoring')->where('competitionId', $check['competitionId'][$i])->delete();
                }
                
                $juror = DB::table('scoring')->select('competitionId')->where('jurorId',$check['jurorId1'] )->get();
            $countJuror = count($juror);
            for ($j=0; $j < $countJuror ; $j++) { 
                $arena = Competition::findOrFail($juror[$j]->competitionId);
                if($arena->arenaId != $check['arenaId'] && ($arena->date = $check['date'] && strtotime($arena->timeStart) == strtotime($check['timeStart']) && strtotime($arena->timeEnd) == strtotime($check['timeEnd']))){
                    echo "masuk";
                    return response()->json([
                        'status' => 'fail',
                        'message' => "in this session jurror 1 is in arena ".$arena->arenaId
                    ]);
                }
            }

            $juror = DB::table('scoring')->select('competitionId')->where('jurorId',$check['jurorId2'] )->get();
            $countJuror = count($juror);
            for ($j=0; $j < $countJuror ; $j++) { 
                $arena = Competition::findOrFail($juror[$j]->competitionId);
                if($arena->arenaId != $check['arenaId'] && ($arena->date = $check['date'] && strtotime($arena->timeStart) == strtotime($check['timeStart']) && strtotime($arena->timeEnd) == strtotime($check['timeEnd']))){
                    return response()->json([
                        'status' => 'fail',
                        'message' => "in this session jurror 2 is in arena ".$arena->arenaId
                    ]);
                }
            }

            $juror = DB::table('scoring')->select('competitionId')->where('jurorId',$check['jurorId3'] )->get();
            $countJuror = count($juror);
            for ($j=0; $j < $countJuror ; $j++) { 
                $arena = Competition::findOrFail($juror[$j]->competitionId);
                if($arena->arenaId != $check['arenaId'] && ($arena->date = $check['date'] && strtotime($arena->timeStart) == strtotime($check['timeStart']) && strtotime($arena->timeEnd) == strtotime($check['timeEnd']))){
                    return response()->json([
                        'status' => 'fail',
                        'message' => "in this session jurror 3 is in arena ".$arena->arenaId
                    ]);
                }
            }

            $juror = DB::table('scoring')->select('competitionId')->where('jurorId',$check['jurorId4'] )->get();
            $countJuror = count($juror);
            for ($j=0; $j < $countJuror ; $j++) { 
                $arena = Competition::findOrFail($juror[$j]->competitionId);
                if($arena->arenaId != $check['arenaId'] && ($arena->date = $check['date'] && strtotime($arena->timeStart) == strtotime($check['timeStart']) && strtotime($arena->timeEnd) == strtotime($check['timeEnd']))){
                    return response()->json([
                        'status' => 'fail',
                        'message' => "in this session jurror 4 is in arena ".$arena->arenaId
                    ]);
                }
            }

            $juror = DB::table('scoring')->select('competitionId')->where('jurorId',$check['jurorId5'] )->get();
            $countJuror = count($juror);
            for ($j=0; $j < $countJuror ; $j++) { 
                $arena = Competition::findOrFail($juror[$j]->competitionId);
                if($arena->arenaId != $check['arenaId'] && ($arena->date = $check['date'] && strtotime($arena->timeStart) == strtotime($check['timeStart']) && strtotime($arena->timeEnd) == strtotime($check['timeEnd']))){
                    return response()->json([
                        'status' => 'fail',
                        'message' => "in this session jurror 5 is in arena ".$arena->arenaId
                    ]);
                }
            }


                $insert = array(
                array('jurorId' => $check['jurorId1'], 'competitionId' => $check['competitionId'][$i], 'jurorNumber' => 1),
                array('jurorId' => $check['jurorId2'], 'competitionId' => $check['competitionId'][$i], 'jurorNumber' => 2),
                array('jurorId' => $check['jurorId3'], 'competitionId' => $check['competitionId'][$i], 'jurorNumber' => 3),
                array('jurorId' => $check['jurorId4'], 'competitionId' => $check['competitionId'][$i], 'jurorNumber' => 4),
                array('jurorId' => $check['jurorId5'], 'competitionId' => $check['competitionId'][$i], 'jurorNumber' => 5)
                );
                $check1 = null;
                $check2 = null;
                $check3 = null;
                $check4 = null;
                $check5 = null;
                if ($check['jurorId1'] == $check['jurorId2'] || $check['jurorId1'] == $check['jurorId3'] || $check['jurorId1'] == $check['jurorId4'] || $check['jurorId1'] == $check['jurorId5'] ){
                    $check1 = 1;
                }
                if ($check['jurorId2'] == $check['jurorId1'] || $check['jurorId2'] == $check['jurorId3'] || $check['jurorId2'] == $check['jurorId4'] || $check['jurorId2'] == $check['jurorId5'] ){
                    $check2 = 1;
                }
                if ($check['jurorId3'] == $check['jurorId2'] || $check['jurorId1'] == $check['jurorId3'] || $check['jurorId3'] == $check['jurorId4'] || $check['jurorId3'] == $check['jurorId5'] ){
                    $check3 = 1;
                }
                if ($check['jurorId4'] == $check['jurorId1'] || $check['jurorId4'] == $check['jurorId2'] || $check['jurorId3'] == $check['jurorId4'] || $check['jurorId4'] == $check['jurorId5'] ){
                    $check4 = 1;
                }
                if ($check['jurorId5'] == $check['jurorId2'] || $check['jurorId1'] == $check['jurorId5'] || $check['jurorId5'] == $check['jurorId4'] || $check['jurorId3'] == $check['jurorId5'] ){
                    $check5 = 1;
                }

                if (!is_null($check1) || !is_null($check2) || !is_null($check3) || !is_null($check4) || !is_null($check5)){
                    return response()->json([
                        'status' => "fail",
                        'messasge' => "juror is alredy in this competition ".$competition->competitionId
                    ]);
                }else{
                $competition->save();
                DB::table('scoring')->insert($insert);
                }
            }

            if ($i>0){
                return response()->json([
                    'status' => 'success',
                    'timeStart' => $check['timeStart'],
                    'timeEnd' => $check['timeEnd'],
                    'date' => $check['date'],
                    'arenaId' => $check['arenaId'],
                    ]);
                }else{
                    return response()->json(['status' => 'fail']);
                }
        }
        
    
    }

    public function showTime(Request $request){
        $arena = $request->input('arena');
        $date = $request->input('date');
        $championship = $request->input('championship');
        $data = DB::table('competition')->select('timeStart', 'timeEnd')
        ->where('championshipId', $championship)
        ->whereNotNull('timeStart')
        ->whereNotNull('timeEnd')
        ->orderBy('timeStart');

        if ($arena !=0){
            $data = $data
            ->where('competition.arenaId', $arena);
        }
        if ($date !=0){
            $data = $data
            ->where('competition.date', $date);
        }
        $data = $data->get();

        $response = [];
        
        $count = count($data);
        for ($i=0; $i < $count; $i++) { 
            # code...
            $list = (object)[];
            $list->timeStart = !empty($data[$i]->timeStart) ? $data[$i]->timeStart:"-";
            $list->timeEnd = !empty($data[$i]->timeEnd) ? $data[$i]->timeEnd:"-";
            
            if(empty($response)){
                $response[] = $list;
            }else{
                
                $countResponse = count($response);
                for ($j=0; $j < $countResponse ; $j++) { 
                    // print_r($response[$j]->timeStart.' | ');
                    // if($response[$j]->round != $list->round 
                    // && $response[$j]->timeStart != $list->timeStart 
                    // && $response[$j]->timeEnd != $list->timeEnd){
                    //     $response[] = $list;
                    // }
                    $string1 = strcmp($response[$j]->timeStart , $list->timeStart);
                    $string2 = strcmp($response[$j]->timeEnd , $list->timeEnd);
                    
                    // echo "i=".$i."\n";
                    // echo "j=".$j."\n";
                    // print_r($response[$j]->timeStart." | ".$list->timeStart." ! \n");
                    // print_r($response[$j]->round." | ".$list->round." ! \n");
                    // print_r($string1." | ".$string2." ! ");
                    
                    // echo "end\n";
                    if($string1 != 0 && $string2 != 0)
                    {
                        
                        $bool = 1;
                    }else{
                        $bool = 0;
                    }
                    
                }
                if($bool == 1){
                    // echo "masuk\n";
                    $response[] = $list;
                }
            }

        }
        return response($response);
    }

    public function showSchedule(Request $request){
        $arena = $request->input('arena');
        $date = $request->input('date');
        $timeEnd = $request->input('timeEnd');
        $timeStart = $request->input('timeStart');
        $championship = $request->input('championship');
        $data = $data = DB::table('v_schedule_list')->select('*');
        

        if ($arena !=0){
            $data = $data
            ->where('arenaId', $arena);
        }
        if ($date !=0){
            $data = $data
            ->where('date', $date);
        }
        if ($timeStart !=0){
            $data = $data
            ->where('timeStart', $timeStart);
        }
        if ($timeEnd !=0){
            $data = $data
            ->where('timeEnd', $timeEnd);
        }
        $data = $data->get();
        if (is_null($data)){
            return response()->json([
                'message' => "There is no schedule"
            ]);
        }else{
            return response($data);
            
            }
            
    }
    
    public function showScheduledCompetition(Request $request){
        $arena = $request->input('arena');
        $date = $request->input('date');
        $timeEnd = $request->input('timeEnd');
        $timeStart = $request->input('timeStart');
        $qualification = $request->input('qualification');
        $championship = $request->input('championship');
        $data = DB::table('competition')
        ->select('competition.competitionId', 'class.className', 'competition.date', 'competition.timeStart', 'competition.timeEnd', 'competition.arenaId', 'competition.scheduleNumber')
        ->where('championshipId', $championship)
        ->whereNotNull('arenaId')
        ->whereNotNull('date')
        ->whereNotNull('timeStart')
        ->whereNotNull('timeEnd')
        ->leftJoin('class', 'class.classId', '=', 'competition.classId');

        if ($arena !=0){
            $data = $data
            ->where('competition.arenaId', $arena);
        }
        if ($qualification !=0){
            $data = $data
            ->where('competition.qualification', $qualification);
        }
        if ($date !=0){
            $data = $data
            ->where('competition.date', $date);
        }
        if ($timeStart !=0){
            $data = $data
            ->where('competition.timeStart', $timeStart);
        }
        if ($timeEnd !=0){
            $data = $data
            ->where('competition.timeEnd', $timeEnd);
        }
        $data = $data->get();
        if(!is_null($data)){
        return response()->json($data);
        }else{
            return response()->json(['status' => 'fail']);
        }
        
    }

    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function showById($id)
    {
        // $competition = new Competition;
        // $competition = Competition::find(1);
        // // echo($id);
        // // var_dump($competition);
        // return response()->json($result);
        return response()->json(Competition::findOrFail($id));
    }

    public function reportWeight($id){
        $data = DB::table('competition')
        ->select( 'competition.competitionId','competition.date','competition.redCorner','competition.blueCorner', 'class.className', 'class.gender', 'competition.qualification', 'competition.competitionNumber',  'contingent.contingentName', 'participant.participantId', 'participant.participantName')
        ->leftJoin('class', 'competition.classId', '=', 'class.classId')
        ->leftjoin('follows','competition.competitionId', '=', 'follows.competitionId')
        ->leftJoin('participant','follows.participantId','=','participant.participantId')
        ->leftJoin('contingent', 'participant.contingentId', '=', 'contingent.contingentId')
        ->where('competition.competitionId', $id)
        ->get();
        $count = count($data);
        $response = [];
        for ($i=0; $i < $count ;$i=$i+2) { 
            # code...
            // do{
            
            $competition = (object)[] ;
            $competition->competitionId = $data[$i]->competitionId;
            $competition->participantName = [];
            $competition->participantName['redCorner'] = ($data[$i]->redCorner = $data[$i]->participantId) ? $data[$i]->participantName : null;
            // $competition->participantName['redCorner'] = (!is_null($data[$i]->redCorner)) ? DB::table('participant')->select('participantName')->where('participantId', $data[$i]->redCorner)->get() : null ;
            $competition->participantName['blueCorner'] = ($data[$i]->redCorner = $data[$i+1]->participantId) ? $data[$i+1]->participantName : null;
            // $competition->participantName['blueCorner'] =(!is_null($data[$i]->blueCorner)) ? DB::table('participant')->select('participantName')->where('participantId', $data[$i]->blueCorner)->get() : null ;
            $competition->gender = $data[$i]->gender;
            $competition->date = $data[$i]->date;
            $competition->qualification = $data[$i]->qualification;
            $competition->winner = $data[$i]->competitionNumber;
            $competition->className = $data[$i]->className;
            
            // $i++;
            echo($i);
            $response[] = $competition;
        }
        return response()->json($response);
    }

    public function getClassChampion(Request $request){
        $championship = $request->input('championship');
        $final = DB::table('competition')->leftJoin('class', 'class.classId', '=', 'competition.classId')->where('nextStage', null)->where('championshipId', $championship)->orderBy('className')->get();
        
        $response = [];
        if ($final){
            foreach ($final as $finals) {
                $winner = (object)[];
                $semiFinal = (object)[];
                if ($finals->winner == $finals->redCorner){ 
                    $winner->championNo1 = DB::table('participant')
                    ->select('participant.participantName', 'contingent.contingentName')
                    ->leftJoin('contingent', 'participant.contingentId', '=', 'contingent.contingentId')
                    ->where('participantId', $finals->winner)->first();
                    $winner->championNo2 = DB::table('participant')
                    ->select('participant.participantName', 'contingent.contingentName')
                    ->leftJoin('contingent', 'participant.contingentId', '=', 'contingent.contingentId')
                    ->where('participantId', $finals->blueCorner)->first();
                    // print_r($winner);
                    // echo('else masuk ');    
                    $semiFinal = DB::table('competition')->where('classId', $finals->classId)->where('bracketNo', $finals->noWinBlue)->first();

                    // print_r($semiFinal->winner);
                    // print_r($semiFinal->redCorner);
                    if ($semiFinal->winner == $semiFinal->redCorner){   
                        // echo('else masuk ');
                        $winner->championNo3 = DB::table('participant')
                        ->select('participant.participantName', 'contingent.contingentName')
                        ->leftJoin('contingent', 'participant.contingentId', '=', 'contingent.contingentId')
                        ->where('participantId', $semiFinal->blueCorner)->first();
                    }else{
                        $winner->championNo3 = DB::table('participant')
                        ->select('participant.participantName', 'contingent.contingentName')
                        ->leftJoin('contingent', 'participant.contingentId', '=', 'contingent.contingentId')
                        ->where('participantId', $semiFinal->redCorner)->first();
                    }

                }else{
                    $winner->championNo1 = DB::table('participant')
                    ->select('participant.participantName', 'contingent.contingentName')
                    ->leftJoin('contingent', 'participant.contingentId', '=', 'contingent.contingentId')
                    ->where('participantId', $finals->winner)->get();
                    $winner->championNo2 = DB::table('participant')
                    ->select('participant.participantName', 'contingent.contingentName')
                    ->leftJoin('contingent', 'participant.contingentId', '=', 'contingent.contingentId')
                    ->where('participantId', $finals->blueCorner)->get();
                    $semiFinal = DB::table('competition')->where('classId', $finals->classId)->where('bracketNo', $finals->noWinBlue)->first();
                    // var_dump($semiFinal);
                    //print_r($semiFinal->winner);
                    // print_r($semiFinal->redCorner);
                    if ($semiFinal->winner == $semiFinal->redCorner){   
                        $winner->championNo3 = DB::table('participant')
                        ->select('participant.participantName', 'contingent.contingentName')
                        ->leftJoin('contingent', 'participant.contingentId', '=', 'contingent.contingentId')
                        ->where('participantId', $semiFinal->blueCorner)->get();
                    }else{
                        $winner->championNo3 = DB::table('participant')
                        ->select('participant.participantName', 'contingent.contingentName')
                        ->leftJoin('contingent', 'participant.contingentId', '=', 'contingent.contingentId')
                        ->where('participantId', $semiFinal->redCorner)->get();
                    }
                }
                $winner->classId = $finals->classId;
                $winner->className = $finals->className;
                $winner->gender = $finals->gender;  
                
                $response[] = $winner;
            }
        }
        return ($response);
    }
    
    /**
     * Retrieve the all user 
     *
     * @return Response
     */

    public function showAllCompetition(){
        return response()->json(Competition::all());
    }

    public function showRoundAndTime(Request $request){
        $arena = $request->input('arena');
        $championship = $request->input('championship');
        date_default_timezone_set("Asia/Bangkok");
        $currentDate =  date("Y-m-d");
        $data = DB::table('competition')->select('qualification', 'timeStart', 'timeEnd', 'date')->where('arenaId', $arena)->where('date', $currentDate)
        ->where('championshipId', $championship)
        ->orderBy('qualification')->get();
        $response = [];
        
        $count = count($data);
        for ($i=0; $i < $count; $i++) { 
            # code...
            $list = (object)[];
            $list->round = $data[$i]->qualification;
            $list->timeStart = !empty($data[$i]->timeStart) ? $data[$i]->timeStart:"-";
            $list->timeEnd = !empty($data[$i]->timeEnd) ? $data[$i]->timeEnd:"-";
            
            if(empty($response)){
                $response[] = $list;
            }else{
                
                $countResponse = count($response);
                for ($j=0; $j < $countResponse ; $j++) { 
                    // print_r($response[$j]->timeStart.' | ');
                    // if($response[$j]->round != $list->round 
                    // && $response[$j]->timeStart != $list->timeStart 
                    // && $response[$j]->timeEnd != $list->timeEnd){
                    //     $response[] = $list;
                    // }
                    $string1 = strcmp($response[$j]->timeStart , $list->timeStart);
                    $string2 = strcmp($response[$j]->timeEnd , $list->timeEnd);
                    
                    // echo "i=".$i."\n";
                    // echo "j=".$j."\n";
                    // print_r($response[$j]->timeStart." | ".$list->timeStart." ! \n");
                    // print_r($response[$j]->round." | ".$list->round." ! \n");
                    // print_r($string1." | ".$string2." ! ");
                    
                    // echo "end\n";
                    if( $response[$j]->round != $list->round  
                    || ($string1 != 0 && $string2 != 0)
                    )
                    {
                        
                        $bool = 1;
                    }else{
                        $bool = 0;
                    }
                    
                }
                if($bool == 1){
                    // echo "masuk\n";
                    $response[] = $list;
                }
            }

        }
        return response($response);
    }

    public function showRoundAndTimeByDate(Request $request){
        $arena = $request->input('arena');
        $championship = $request->input('championship');
        $dataTgl = $request->input('date');
        date_default_timezone_set("Asia/Bangkok");
        $currentDate =  date("Y-m-d");
        $data = DB::table('competition')->select('qualification', 'timeStart', 'timeEnd', 'date')->where('arenaId', $arena)->where('date', $dataTgl)
        ->where('championshipId', $championship)
        ->orderBy('qualification')->get();
        $response = [];
        
        $count = count($data);
        for ($i=0; $i < $count; $i++) { 
            # code...
            $list = (object)[];
            $list->round = $data[$i]->qualification;
            $list->timeStart = !empty($data[$i]->timeStart) ? $data[$i]->timeStart:"-";
            $list->timeEnd = !empty($data[$i]->timeEnd) ? $data[$i]->timeEnd:"-";
            
            if(empty($response)){
                $response[] = $list;
            }else{
                
                $countResponse = count($response);
                for ($j=0; $j < $countResponse ; $j++) { 
                    $string1 = strcmp($response[$j]->timeStart , $list->timeStart);
                    $string2 = strcmp($response[$j]->timeEnd , $list->timeEnd);
                    
                    if( $response[$j]->round != $list->round  
                    || ($string1 != 0 && $string2 != 0)
                    )
                    {
                        
                        $bool = 1;
                    }else{
                        $bool = 0;
                    }
                    
                }
                if($bool == 1){
                    $response[] = $list;
                }
            }

        }
        return response($response);
    }

    public function showCompetitionMonitoringByDate(Request $request){
        
        $class = $request->input('class');
        $date = $request->input('date');
        $timseStart = $request->input('timeStart');
        $timeEnd = $request->input('timeEnd');
        $championship = $request->input('championship');
        date_default_timezone_set("Asia/Bangkok");
        $currentDate =  $request->input('date');
        $arena = $request->input('arena');
        $qualification = $request->input('qualification');
        $data =  DB::table('competition')->select('competition.competitionId', 'class.className', 'competition.redCorner', 'competition.blueCorner', 'competition.winner','competition.date','class.gender','competition.finalScore')
        ->leftJoin('class','competition.classId','=','class.classId')
        ->where('championshipId', $championship)
        ->where('competition.date', $currentDate);
        
        // var_dump($currentDate);

        if ($arena != 0) {
            $data = $data
            ->where('competition.arenaId', $arena);
        }
        if ($class !=0){
            $data = $data
            ->where('competition.classId', $class);
        }
        if ($qualification !=0){
            $data = $data
            ->where('competition.qualification', $qualification);
        }
        if ($timseStart !=0){
            $data = $data
            ->where('competition.timeStart', $timseStart);
        }
        if ($timeEnd !=0){
            $data = $data
            ->where('competition.timeEnd', $timeEnd);
        }
        // if ($date !=0){
        //     $data = $data
        //     ->where('competition.date', $date);
        // }
        $data = $data->orderby('competition.scheduleNumber')->get();
        // var_dump($data[0]->date);
        $count = count($data);
        if($count == 0){
            return response()->json(['message' => "there is no matches in this schedule"]);
        }else{
            $response = [];

            for ($i=0; $i < $count ; $i++) { 
                
                $winner = DB::table('participant')->select('participantName')->where('participantId', $data[$i]->winner)->first();
                $competition = (object)[] ;
                $competition->competitionId = $data[$i]->competitionId;
                $competition->className = $data[$i]->className;
                $competition->winner = $data[$i]->winner;
                $competition->winnerName = (!is_null($winner)) ? $winner->participantName : null;
                $competition->redCorner = [];
                $competition->blueCorner = [];
                // $competition->participantName['redCorner'] = ($data[$i]->redCorner = $data[$i]->participantId) ? $data[$i]->participantName : null;
                // $participantRed = [];
                if( !is_null($data[$i]->redCorner)){
                    $participantRed = DB::table('participant')->select('participant.participantName', 'contingent.contingentName')->where('participantId', $data[$i]->redCorner)->leftjoin('contingent','contingent.contingentId', '=', 'participant.contingentId')->first();
                    $competition->redCorner['participantName'] = (!is_null($participantRed->participantName)) ? $participantRed->participantName : 'bye';
                    $competition->redCorner['contingentName'] = $participantRed->contingentName;
                }else{
                    $competition->redCorner['participantName'] = "BYE";
                }
                if( !is_null($data[$i]->blueCorner )){
                $participantBlue = DB::table('participant')->select('participant.participantName', 'contingent.contingentName')->where('participantId', $data[$i]->blueCorner)->leftjoin('contingent','contingent.contingentId', '=', 'participant.contingentId')->first();
                $competition->blueCorner['participantName'] = (!is_null($participantBlue->participantName)) ? $participantBlue->participantName : 'bye';
                $competition->blueCorner['contingentName'] = $participantBlue->contingentName;
                }else{
                    $competition->blueCorner['participantName'] = "BYE";
                }
                
                $response[] = $competition;

            }

            return response()->json($response);
        }
    }

    public function showCompetitionMonitoring(Request $request){
        
        $class = $request->input('class');
        $date = $request->input('date');
        $timseStart = $request->input('timeStart');
        $timeEnd = $request->input('timeEnd');
        $championship = $request->input('championship');
        date_default_timezone_set("Asia/Bangkok");
        $currentDate =  date("Y-m-d");
        $arena = $request->input('arena');
        $qualification = $request->input('qualification');
        $data =  DB::table('competition')->select('competition.competitionId', 'class.className', 'competition.redCorner', 'competition.blueCorner', 'competition.winner','competition.date', 'competition.finalScore')
        ->leftJoin('class','competition.classId','=','class.classId')
        ->where('championshipId', $championship)
        ->where('competition.date', $currentDate);
        
        // var_dump($currentDate);

        if ($arena != 0) {
            $data = $data
            ->where('competition.arenaId', $arena);
        }
        if ($class !=0){
            $data = $data
            ->where('competition.classId', $class);
        }
        if ($qualification !=0){
            $data = $data
            ->where('competition.qualification', $qualification);
        }
        if ($timseStart !=0){
            $data = $data
            ->where('competition.timeStart', $timseStart);
        }
        if ($timeEnd !=0){
            $data = $data
            ->where('competition.timeEnd', $timeEnd);
        }
        // if ($date !=0){
        //     $data = $data
        //     ->where('competition.date', $date);
        // }
        $data = $data->orderby('competition.scheduleNumber')->get();
        // var_dump($data[0]->date);
        $count = count($data);
        if($count == 0){
            return response()->json(['message' => "there is no matches in this schedule"]);
        }else{
            $response = [];

            for ($i=0; $i < $count ; $i++) { 
                $winningBy = DB::table('detail_competition')->where('competitionId', $data[$i]->competitionId)->whereNotNull('winStatus')->first();
                $winner = DB::table('participant')->select('participantName')->where('participantId', $data[$i]->winner)->first();
                $competition = (object)[] ;
                $competition->competitionId = $data[$i]->competitionId;
                $competition->className = $data[$i]->className;
                $competition->winner = $data[$i]->winner;
                $competition->winnerSide = is_null($data[$i]->winner) ? null : $data[$i]->winner == $data[$i]->blueCorner ? "BLUE" : $data[$i]->winner == $data[$i]->redCorner ? "RED" : null;
                $competition->winStatus = is_null($winningBy) ? null : $winningBy->winStatus;
                $competition->finalScore = $data[$i]->finalScore;
                $competition->winnerName = (!is_null($winner)) ? $winner->participantName : null;
                $competition->redCorner = [];
                $competition->blueCorner = [];
                if( !is_null($data[$i]->redCorner)){
                    $participantRed = DB::table('participant')->select('participant.participantName', 'contingent.contingentName')->where('participantId', $data[$i]->redCorner)->leftjoin('contingent','contingent.contingentId', '=', 'participant.contingentId')->first();
                    $competition->redCorner['participantName'] = (!is_null($participantRed->participantName)) ? $participantRed->participantName : 'bye';
                    $competition->redCorner['contingentName'] = $participantRed->contingentName;
                }else{
                    $competition->redCorner['participantName'] = "BYE";
                }
                if( !is_null($data[$i]->blueCorner )){
                $participantBlue = DB::table('participant')->select('participant.participantName', 'contingent.contingentName')->where('participantId', $data[$i]->blueCorner)->leftjoin('contingent','contingent.contingentId', '=', 'participant.contingentId')->first();
                $competition->blueCorner['participantName'] = (!is_null($participantBlue->participantName)) ? $participantBlue->participantName : 'bye';
                $competition->blueCorner['contingentName'] = $participantBlue->contingentName;
                }else{
                    $competition->blueCorner['participantName'] = "BYE";
                }
                
                $response[] = $competition;

            }

            return response()->json($response);
        }
    }

    /**
     * Retrieve the user for the given ID.
     *
     * @param Request $request
     * @return Response
     */
    public function showCompetitionBy(Request $request){
        
        $participant = $request->input('participantName');
        $arena = $request->input('arena');
        $class = $request->input('class');
        $scheme = $request->input('scheme');
        $id = $request->input('id');
        $championship = $request->input('championship');
        $qualification = $request->input('qualification');
        $data =  DB::table('competition')
        // ->leftjoin('follows','competition.competitionId', '=', 'follows.competitionId')
        ->leftJoin('arena','competition.arenaId', '=', 'arena.arenaId')
        // ->leftJoin('participant','follows.participantId','=','participant.participantId')
        ->leftJoin('class','competition.classId','=','class.classId')
        ->where('championshipId', $championship);
        
        
        // ->where('participant.participantName', 'like', "%".$participant."%");
        if ($arena != 0) {
            $data = $data
            ->where('competition.arenaId', $arena);
        }
        if ($participant !=0){
            $data = $data
            ->where('participant.participantName', 'like', "%".$participant."%");
        }
        if ($class !=0){
            $data = $data
            ->where('competition.classId', $class);
        }
        if ($scheme !=0){
            $data = $data
            ->where('competition.schemeId', $scheme);
        }
        if ($id !=0){
            $data = $data
            ->where('competition.competitionId', $id);
        }
        if ($qualification !=0){
            $data = $data
            ->where('competition.qualification', $qualification);
        }
        $data = $data->orderby('competition.competitionId')->get();
        // print_r($data);
        $count = count($data);
        $response = [];
        // print_r($data);
        $seed = 1;
        for ($i=0; $i < $count ;$i++) { 
            # code...
            // do{
            
            $competition = (object)[] ;
            $competition->competitionId = $data[$i]->competitionId;
            $competition->participantName = [];
            // $competition->participantName['redCorner'] = ($data[$i]->redCorner = $data[$i]->participantId) ? $data[$i]->participantName : null;

            $competition->participantName['redCorner'] = [];
            $competition->participantName['blueCorner'] = [];
            $participantRed = DB::table('participant')->select('participantName')->where('participantId', $data[$i]->redCorner)->get();
            $participantBlue = DB::table('participant')->select('participantName')->where('participantId', $data[$i]->blueCorner)->get();
            
            if (is_null($participantRed) && is_null($participantBlue)){
                $competition->participantName['redCorner']= $participantRed;
                // $competition->participantName['seedRed'] = $seed;
                // $seed++;
                $competition->participantName['blueCorner'] = $participantBlue;
                // $competition->participantName['seedBlue'] = $seed;
                // $seed++;
            }else{
            $competition->participantName['redCorner'] = (!is_null($participantRed)) ? $participantRed : ("BYE") ;
            // $competition->participantName['seedRed']= $seed;
            // $seed++;
            // $competition->participantName['blueCorner'] = ($data[$i]->redCorner = $data[$i+1]->participantId) ? $data[$i+1]->participantName : null;
            $competition->participantName['blueCorner'] = (!is_null($participantBlue)) ? $participantBlue : ("BYE") ;
            // $competition->participantName['seedBlue'] = $seed;
            // $seed++;
            }
            $competition->timeStart = $data[$i]->timeStart;
            $competition->timeEnd = $data[$i]->timeEnd;
            $competition->date = $data[$i]->date;
            $competition->qualification = $data[$i]->qualification;
            $competition->winner = $data[$i]->winner;
            $competition->finalScore = $data[$i]->finalScore;
            $competition->arenaName = $data[$i]->arenaName;
            $competition->className = $data[$i]->className;
            $competition->nextStage = $data[$i]->nextStage;
            $competition->lastStage = json_decode($data[$i]->lastStage);
            $competition->bracketNo = $data[$i]->bracketNo;
            $competition->competitionNumber = $data[$i]->competitionNumber;
            
            // $i++;
            // echo($i);
            $response[] = $competition;
        }
        // }while ($i <= $tes);
        return response()->json($response);
    }


    public function showCompetitionScheme($scheme){

        $data = DB::table('v_competition')->select('*')
        ->where('schemeId',$scheme)
        ->get();
        $count = count($data);
        
        $response = [];
        $match = [];
        $seed = 1;
        $competitionId = 0;
        $roundId = 0;
        $comp_temp = [];
        $round = [];
        $temp = 0;
        $first_round = TRUE;
        // print_r('<pre>');
        // print_r($data);
        // print_r('</pre>');die();
        for ($i=0; $i < $count ; $i++) { 
            
            //red corner
            $competition = (object)[] ;
            if($first_round && empty($data[$i]->red_name) && $roundId == $data[$i]->qualification){
                $competition->name = 'BYE';
            }else{
                $competition->name = (empty($data[$i]->red_name) ? "_________________" :$data[$i]->red_name);
            }
            
            $competition->id = $data[$i]->red_id;
            $competition->seed = $data[$i]->red_id;
            $competition->round = $data[$i]->qualification;
            $match[] = $competition;

            //blue coner
            $competition = (object)[] ;
            if($first_round && empty($data[$i]->blue_name) && $roundId == $data[$i]->qualification){
                $competition->name = 'BYE';
            }else{
                $competition->name = (empty($data[$i]->blue_name) ? "_________________" :$data[$i]->blue_name);
            }

            $competition->id = $data[$i]->blue_id;
            $competition->seed = $data[$i]->red_id;
            $competition->round = $data[$i]->qualification;
            $match[] = $competition;


            if ($roundId == 0) {
                $roundId = $data[$i]->qualification;
            }

            if($roundId != $data[$i]->qualification){
                $response[] = $round;
                $roundId = $data[$i]->qualification;
                $round = null;
                $first_round = FALSE;
            }

            // add tu round
            $round[] = $match;
            $match = null;

            if($i == ($count - 1)){
                $response[] = $round;
                $roundId = $data[$i]->qualification;
                $round = null;

                //winner
                $competition = (object)[] ;
                $competition->name = (empty($data[$i]->winner_name) ? "_________________" :$data[$i]->winner_name);

                $competition->id = $data[$i]->winner;
                $competition->seed = $data[$i]->winner;
                $competition->round = 'WIN';
                $match[] = $competition;
                // add tu round
                $round[] = $match;
                $match = null;

                $response[] = $round;

            }

        }
        return response()->json($response);

    }


    // }
    /**
     * Update the user for the given ID.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return Response
     */
    public function update($id, Request $request){
        $competition = Competition::findOrFail($id);
        $competition->competitionName = $request->input('competitionName');
        
        if ($competition->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }

    }

    public function updateSchedule(Request $request){
        $competition = $request->json('competitionId');
        $response = TRUE;
        DB::beginTransaction();
        try{
            foreach ($competition as $value) {
                $competition = Competition::findOrFail($value);
                $competition->timeStart = $request->json()->get('timeStart');
                $competition->timeEnd = $request->json()->get('timeEnd');
                $competition->date = $request->json()->get('date');
                $competition->save();
            }

            DB::commit();
        } catch (\Exception $e) {
            $response = FALSE;
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            $response = FALSE;
            DB::rollback();
            throw $e;
        }
        if($response){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail', 'msg' => 'Jadwal gagal diedit !']);
        }

    }



    /**
     * Delete the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id){
        Competition::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    public function deleteSchedule(Request $request){
        DB::beginTransaction();
        $response = TRUE;
        try{
            $getCompetition = DB::table('competition')->select('competitionId')
            ->where('date', $request->json()->get('date'))
            ->where('timeStart', $request->json()->get('timeStart'))
            ->where('timeEnd', $request->json()->get('timeEnd'))
            ->where('arenaId', $request->json()->get('arenaId'))->get();
            DB::table('competition')
            ->where('date', $request->json()->get('date'))
            ->where('timeStart', $request->json()->get('timeStart'))
            ->where('timeEnd', $request->json()->get('timeEnd'))
            ->where('arenaId', $request->json()->get('arenaId'))
            ->update(['date' => null, 'timeStart' => null, 'timeEnd' => null, 'arenaId' => null]);
            $count = count($getCompetition);
            for ($i=0; $i < $count ; $i++) { 
                $scoring = DB::table('scoring')
                ->where('competitionId', $getCompetition[$i]->competitionId)
                ->delete();
            }
            
            DB::commit();
        } catch (\Exception $e) {
            $response = FALSE;
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            $response = FALSE;
            DB::rollback();
            throw $e;
        }
        if($response){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail', 'msg' => 'Jadwal gagal diedit !']);
        }
    }

    public function showRound(Request $request){
        $class = $request->input('class');
        $data = DB::table('competition')->select('qualification')->where('classId', $class)->get();
        $response = [];
        $check = [];
        $count = count($data);
        // print_r($count);

        for ($i=0; $i < $count ; $i++) { 
            $list = [];
            $list['round'] = $data[$i]->qualification;
            if(empty($check)){
                $check[] = $list;
            }else{
                
                $countResponse = count($check);
                for ($j=0; $j < $countResponse ; $j++) { 
                    if( $check[$j]['round'] != $list['round'])
                    {  
                        $bool = 1;
                    }else{
                        $bool = 0;
                    }
                    
                }
                if($bool == 1){
                    // echo "masuk\n";
                    $check[] = $list;
                }
            }
            
        }
        $countCheck = count($check);
        // print_r($countCheck);
        for ($i=0; $i < $countCheck ; $i++) { 
            
            $qualification = (object)[];
            $qualification->round = $check[$i]['round'];
            if($check[$i]['round'] == $countCheck){
                $qualification->roundName = "FINAL";
            }elseif($check[$i]['round'] == $countCheck-1){
                $qualification->roundName = "SEMIFINAL";
            }elseif($check[$i]['round'] == $countCheck-2){
                $qualification->roundName = "QUARTER FINAL";
            }else{
                $qualification->roundName = "Round ".($i+1);
            }
            $response[] = $qualification;
        }
        return response($response);
    }
}

<?php

namespace App\Http\Controllers;

use App\Classy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClassyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $class = new Classy;

        $class->className = $request->input('className');
        $class->gender = $request->input('gender');
        $class->groupId = $request->input('groupId');
        
        if($class->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
    }

    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function showById($id)
    {
        $data = DB::table('class')
        ->select('class.*', 'groupe.*')->leftjoin('groupe', 'groupe.groupId', '=', 'class.groupId')
        ->where('classId', $id)->first();
        return response()->json($data);
    }
    
    /**
     * Retrieve the all user 
     *
     * @return Response
     */
    public function showAllClass()
    {
        return response()->json(Classy::all());
    }

    public function showClassByName($name){
        $data  = DB::table('class')->where('className', 'LIKE' , "%".$name."%")->get();
        $count = count($data);
        $response = [];
        for ($i=0; $i < $count ; $i++) { 
            $class = (object)[];
            $class->classId = $data[$i]->classId;
            $class->className = $data[$i]->className;
            $response[] = $class;
        }

        return response()->json($response);
    }

    public function showClassByGroupId($id){
        $data1 = DB::table('class')->leftJoin('groupe', 'groupe.groupId', '=', 'class.groupId')->where('class.groupId', $id)->get();
        return response()->json($data1);
    }

    public function showClassByGender($gender){
        // print_r($gender);
        $data = DB::table('class')
        ->select('class.*', 'groupe.*')->leftjoin('groupe', 'groupe.groupId', '=', 'class.groupId')
        ->where('gender', $gender)->get();
        $count = count($data);
        $response = [];
        for ($i=0; $i < $count ; $i++) { 
            $class = (object)[];
            $class->classId = $data[$i]->classId;
            $class->className = $data[$i]->className." - ".$data[$i]->groupName;
            $class->minWeight = $data[$i]->minWeight;
            $class->maxWeight = $data[$i]->maxWeight;
            $class->gender = $data[$i]->gender;
            $class->minAge = $data[$i]->minAge;
            $class->maxAge = $data[$i]->maxAge;
            $response[] = $class;
        }

        return response()->json($response);
    }

    /**
     * Update the user for the given ID.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $class = Classy::findOrFail($id);
        $class->className = $request->json()->get('className');
        $class->competitionId = $request->json()->get('competitionId');
        $class->groupId = $request->json()->get('groupId');
        
        if($class->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
        

    }

    /**
     * Delete the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        Classy::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}

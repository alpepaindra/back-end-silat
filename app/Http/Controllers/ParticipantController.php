<?php

namespace App\Http\Controllers;

use App\Participant;
use App\Contingent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ParticipantController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        
        foreach ($request->json()->all() as $key => $value) {
            // foreach ($object[0] as $key => $value) {
            print_r($request->json()->all());
            $participant = new Participant;
            $participant->participantName = $value['participantName'];
            $participant->contingentId = $value['contingentId'];
            $participant->classId = $value['classId'];
            $participant->officialId = $value['officialId'];
            $participant->age = $value['age'];
            $participant->gender = $value['gender'];
            $participant->placeOfBirth = $value['placeOfBirth'];
            $participant->dateOfBirth = $value['dateOfBirth'];
            $participant->height = $value['height'];
            $participant->weight = $value['weight'];
            $participant->address = $value['address'];
            $array = $participant->toArray();

            // DB::table('participant')->insert($array);
            
            // $participant->save();
        }
        // 
        
      
        
        if (Participant::insert($request->json()->all())){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
    }

    public function saveParticipant(Request $request)
    {
    
        $official = DB::table('contingent')->select('officialId')->where('contingentId', $request->json()->get('contingentId'))->first();
        $participant = new Participant;
        $participant->participantName = $request->json()->get('participantName');
        $participant->contingentId = $request->json()->get('contingentId');
        $participant->classId = $request->json()->get('classId');
        $participant->officialId = $official->officialId;
        $participant->age = $request->json()->get('age');
        $participant->gender = $request->json()->get('gender');
        $participant->placeOfBirth = $request->json()->get('placeOfBirth');
        $participant->dateOfBirth = $request->json()->get('dateOfBirth');
        $participant->height = $request->json()->get('height');
        $participant->weight = $request->json()->get('weight');
        $participant->address = $request->json()->get('address');
        // $array = $participant->toArray();

        // DB::table('participant')->insert($array);
        
        // $participant->save();
        $check = DB::table('participant')->where('contingentId', $participant->contingentId)->where('classId', $participant->classId)->first(); 
        if(!is_null($check)){
            return response()->json(['status' => 'fail', 'message' => "there is already participant in class ".$request->json()->get('classId')]);
        }else{
            if ($participant->save()){
                $contingent  = Contingent::findOrFail($request->json()->get('contingentId'));
                $contingent->participantAmount = $contingent->participantAmount + 1;
                $contingent->save();
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'fail']);
            }
        }
    }



    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function showById($id)
    {
        $check =  DB::table('participant')
        ->select('participant.*', 'class.className')
        ->leftJoin('class','participant.classId','=','class.classId')
        ->where('participantId', $id)
        ->first();
        return response()->json($check);
    }

/**
     * Retrieve the user for the given ID.
     *
     * @param Request $request
     * @return Response
     */
    public function showParticipantBy(Request $request)
    {
        $participant = [];
        $name = $request->input('name');
        $gender = $request->input('gender');
        $contingent = $request->input('contingent');
        $class = $request->input('class');
        $className = $request->input('className');
        $champion = $request->input('championship');
        $sortBy = $request->input('sortBy');
        $orderBy = $request->input('orderBy');
        $check =  DB::table('participant')
        ->select('participant.*', 'contingent.contingentName', 'class.className')
        ->leftJoin('contingent','participant.contingentId', '=', 'contingent.contingentId')
        ->leftJoin('bagian','contingent.contingentId', '=', 'bagian.contingentId')
        ->leftJoin('class','participant.classId','=','class.classId')
        ->where('participantName', 'LIKE' , "%".$name."%")
        ->where('bagian.championshipId', $champion);
        if(!is_null($gender)){
            $check = $check
        ->where('participant.gender', 'like', $gender."%");
        }
        if ($contingent != 0) {
            $check = $check
            
            ->where('participant.contingentId', $contingent);
        }
        if ($class !=0){
            $check = $check
            ->where('participant.classId', $class);
        }
        if ($className !=null){
            $check = $check
            ->where('class.className', 'like', $className);
        }
        if($sortBy != null || $orderBy != null){
            $check = $check->orderBy($sortBy, $orderBy);
        }
        $check = $check->get();
        $participant= $check;
        return response()->json($participant);

    }

    
    /**
     * Retrieve the all user 
     *
     * @return Response
     */
    public function showAllParticipant()
    {
        return response()->json(Participant::all());
    }

    /**
     * Update the user for the given ID.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        // foreach ($request->json()->all() as $key => $value){
        $participant = Participant::findOrFail($id);
        if($participant->classId == $request->json()->get('classId')){
            $bool = 1;
        }else{
            $bool = 0;
        }
        $participant->participantName = $request->json()->get('participantName');
        $participant->classId = $request->json()->get('classId');
        $participant->age = $request->json()->get('age');
        $participant->gender = $request->json()->get('gender');
        $participant->placeOfBirth = $request->json()->get('placeOfBirth');
        $participant->dateOfBirth = $request->json()->get('dateOfBirth');
        $participant->height = $request->json()->get('height');
        $participant->weight = $request->json()->get('weight');
        $participant->address = $request->json()->get('address');
        // echo($participant);
        // }
        $check = DB::table('participant')->where('contingentId', $participant->contingentId)->where('classId', $participant->classId)->first(); 
        print_r($check);
        if(!is_null($check) && $bool == 0){
            return response()->json(['status' => 'fail', 'message' => "there is already participant in class ".$request->json()->get('classId')]);
        }else{
            if ($participant->save()){
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'fail']);
            }
        }
        

        // return response()->json($participant, 200);
    }

    /**
     * Delete the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        Participant::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
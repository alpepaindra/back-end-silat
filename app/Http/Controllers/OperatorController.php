<?php

namespace App\Http\Controllers;

use App\Operator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class OperatorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $operator = new Operator;

        $operator->operatorName = $request->input('name');
        $operator->operatorId = $request->input('id');
        $operator->competitionId = $request->input('username');
        $operator->competitionId = $request->input('password');
        var_dump($request->input('name'));
        
        $operator->save();
    }

    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function showById($id)
    {
        // $operator = new Operator;
        // $operator = Operator::find(1);
        // // echo($id);
        // // var_dump($operator);
        // return response()->json($result);
        return response()->json(Operator::findOrFail($id));
    }
    
    /**
     * Retrieve the all user 
     *
     * @return Response
     */
    public function showAllOperator()
    {
        return response()->json(Operator::all());
    }

    /**
     * Update the user for the given ID.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $operator = Operator::findOrFail($id);
        $operator->operatorName = $request->input('operatorName');
        $operator->competitionId = $request->input('competitionId');
        $operator->competitionId = $request->input('username');
        $operator->competitionId = $request->input('password');
        $operator->save();
        

        return response()->json($operator, 200);
    }

    /**
     * Delete the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        Operator::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    /**
 
    * Display a listing of the resource.
 
    *
 
    * @return \Illuminate\Http\Response
 
    */
 
   public function authenticate(Request $request)
 
   {
 
       $this->validate($request, [
 
       'username' => 'required',
 
       'password' => 'required'
 
        ]);
 
      $operator = Operator::where('username', $request->input('username'))->first();
      echo($operator->password);
      echo(" ");
      echo($request->input('password'));
      $check = Hash::check($request->input('password'), $operator->password);

      if ($request->input('password') == $operator->password){
 
    //  if(Hash::check($request->input('password'), $operator->password)){
    
 
          $apikey = base64_encode(str_random(40));
 
          Operator::where('username', $request->input('username'))->update(['api_key' => "$apikey"]);;
 
          return response()->json(['status' => 'success','api_key' => $apikey]);
 
      }else{
 
          return response()->json(['status' => 'fail'],401);
 
      }
 
   }
}

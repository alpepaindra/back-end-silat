<?php

namespace App\Http\Controllers;

use App\Operator;
use App\Official;
use App\Juror;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class LoginController extends Controller{

    public function login(Request $request, $username, $password){

        // $this->validate($request, [
 
        //     'username' => 'required',
      
        //     'password' => 'required'
      
        //      ]);
            
            //  $operator = Operator::where('username', $request->input('username'))->first();
             $operator = Operator::where('username', $username)->first();
            // if($operator != null && $request->input('password') == $operator->password){
            if($operator != null && $password == $operator->password){
                return response()->json([
                    'status' => 'success', 
                    'loginAs' => 'operator',
                    'operatorId' => $operator->operatorId ,
                    'username' => $operator->username,
                    'password' => $operator->password
                ]);
            }else{
                // $official = Official::where('username', $request->input('username'))->first();
                $official = Official::where('username', $username)->first();
                // if($official != null && $request->input('password') == $official->password){
                if($official != null && $password == $official->password){
                    return response()->json([
                        'status' => 'success',
                        'loginAs' => 'official',
                        'officialId' => $official->officialId,
                        'officialName' => $official->officialName,
                        'username' => $official->username,
                        'password' => $official->password
                    ]);
                }else{
                    // $juror = Juror::where('username', $request->input('username'))->first();
                    $juror = Juror::where('username', $username)->first();
                    // if($juror != null && $request->input('password') == $juror->password){
                    if($juror != null && $password == $juror->password){
                        $competitionNumber = DB::table('competition')->select('competition.competitionId', 'competition.arenaId')
                        ->leftJoin('scoring','competition.competitionId', '=', 'scoring.competitionId')
                        ->leftJoin('juror','juror.jurorId', '=', 'scoring.jurorId')
                        ->where('scoring.jurorId', $juror->jurorId)
                        ->whereNull('competition.winner')
                        ->orderBy('competition.competitionId', 'ASC')
                        ->get();

                        // $arena = DB::table('arena')->select('competition.competitionId')
                        // ->leftJoin('scoring','competition.competitionId', '=', 'scoring.competitionId')
                        // ->leftJoin('juror','juror.jurorId', '=', 'scoring.jurorId')
                        // ->where('scoring.jurorId', $juror->jurorId)
                        // ->get();

                        return response()->json([
                            'status' => 'success',
                            'loginAs' => 'juror',
                            'competitionNumber' => $competitionNumber,
                            'jurorId' => $juror->jurorId,
                            'username' => $juror->username,
                            'password' => $juror->password
                        ]);

                    }else{
                        return response()->json(['status' => 'fail', 'message' => "username or Password is wrong"]);
                    }
                }   
            }
    }
}
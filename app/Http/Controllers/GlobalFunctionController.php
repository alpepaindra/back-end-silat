<?php

namespace App\Http\Controllers;

use App\Juror;
use App\GrandJuror;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GlobalFunctionController extends Controller
{
    public function setWinner($warna)
    {
        if ($warna == 'merah') {
            $pemenang = $this->sudut_merah;
        }
        if ($warna == 'biru') {
            $pemenang = $this->sudut_biru;
        }
        $this->peserta_id_pemenang = $pemenang ? $pemenang->id : null;
        $detail_pertandingan = $this->pertandingan->detail_pertandingan;
        $this->winning_by = $detail_pertandingan['winner']['winning_by'];
        $this->points_red = $detail_pertandingan['winner']['winning_by_points_red'];
        $this->points_blue = $detail_pertandingan['winner']['winning_by_points_blue'];
        $this->save();
        $update_jadwal_pertandingan = static::where('no_bracket_win_merah',$this->no_bracket)->where('kelas_jenis_kelamin',$this->kelas_jenis_kelamin)->first();
        if ($update_jadwal_pertandingan) {
            $update_jadwal_pertandingan->peserta_id_sudut_merah = $pemenang->id;
            $update_jadwal_pertandingan->save();
            $update_jadwal_pertandingan->makePertandingan();
        }
        $update_jadwal_pertandingan = static::where('no_bracket_win_biru',$this->no_bracket)->where('kelas_jenis_kelamin',$this->kelas_jenis_kelamin)->first();
        if ($update_jadwal_pertandingan) {
            $update_jadwal_pertandingan->peserta_id_sudut_biru = $pemenang->id;
            $update_jadwal_pertandingan->save();
            $update_jadwal_pertandingan->makePertandingan();
        }
    }

public function generateBracket($base) {
    $knownBrackets   = collect([2,4,8,16,32,64]);
    $closest         = $knownBrackets[$knownBrackets->search(function($item, $key) use ($base) {
        return $item >= $base;
    })];
    
    $byes            = $closest-$base;
        
    if($byes>0) $base = $closest;

    $brackets    = collect([]);
    $round       = 1;
    $baseT       = $base/2;
    $baseC       = $base/2;

    //variabel untuk menentukan next stage
    $nextInc     = $base/2;
    
        
    for($i=1;$i<=($base-1);$i++) {
        $baseR = $i/$baseT;
        $isBye = false;

        // echo "isBye = ".$isBye." \n" ;
        // echo "byes = ".$byes." \n" ;
        // echo "baseR = ".$baseR." \n" ;
        // echo "baseT = ".$baseT." \n" ;
        // echo "baseC = ".$baseC." \n" ;
        // echo "nextInc = ".$nextInc." \n";
        // echo "round = ".$round." \n";
        // echo "-------------------------\n";
            
        if($byes>0 && ($i%2!=0 || $byes>=($baseT-$i))) {
            $isBye = true;
            $byes--;
        }
        
        $last = $brackets->where('nextGame', $i)->values();
        
        $brackets->push([
            'lastGames'  => $round==1 ? null : count($last) ? [$last[0],$last[1]] : null,
            'nextGame'   => $nextInc+$i>$base-1?null:$nextInc+$i,
            'bracketNo'  => $i,
            'roundNo'    => $round,
            'bye'        => $isBye
        ]);
        if($i%2!=0) $nextInc--;
        while($baseR >=1) {
            $round++;
            $baseC/= 2;
            $baseT = $baseT + $baseC;
            $baseR = $i/$baseT;
        }

        
    }
    
    return $brackets;
}

    public function showAllJurorAndGrandJuror(Request $request){
        $name = $request->input('name');
        $role = $request->input('role');
        $gender = $request->input('gender');
        $championship = $request->input('championship');
        $check = DB::table('v_juror_all')->select('*')
        ->where('championshipId', $championship);
        
        if ($role !=null){
            $check = $check
            ->where('role', 'like', $role."%");
        }
        if ($name !=null){
            $check = $check
            ->where('jurorName', 'like', "%".$name."%");
        }
        if ($gender !=null){
            $check = $check
            ->where('gender', 'like', $gender."%");
        }
        
        
        $check = $check->get();
        return response($check);
    }

    public function editJurorGrandJuror(Request $request){
        $role = $request->json()->get('role');
        $jurorId = $request->json()->get('jurorId');
        if ($role == "JUROR"){
            $juror = Juror::findOrFail($jurorId);
            $juror->jurorName = $request->json()->get('jurorName');
            $juror->role = $request->json()->get('role');
            $juror->gender = $request->json()->get('gender');

            if ($juror->save()){
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'fail']);
            }
        }elseif($role == "GRAND JUROR"){
            $grandJuror = GrandJuror::findOrFail($jurorId);
            $grandJuror->grandJurorName = $request->json()->get('jurorName');
            $grandJuror->role = $request->json()->get('role');
            $grandJuror->gender = $request->json()->get('gender');

            
            if ($grandJuror->save()){
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'fail']);
            }
        }else{
            return response()->json(['status' => 'fail']);
        }


    }

    public function showByIdJurorGrandJuror(Request $request){
        $role = $request->input('role');
        $jurorId = $request->input('jurorId');
        if ($role == "JUROR"){
            $juror = Juror::findOrFail($jurorId);

            return response($juror);
        }elseif($role == "GRAND JUROR"){
            $grandJuror = GrandJuror::findOrFail($jurorId);
            $data = (object)[];
            $data->jurorName = $grandJuror->grandJurorName;
            $data->role = $grandJuror->role;
            $data->gender = $grandJuror->gender;
            
            return response()->json($data);
        }


    }
}
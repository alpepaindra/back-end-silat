<?php

namespace App\Http\Controllers;

use App\Scheme;
use App\Helper\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SchemeController extends Controller
{
    
    
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $scheme = new Scheme;

        $scheme->schemeName = $request->input('schemeName');
        $scheme->schemeId = $request->input('schemeId');
        print($request->getMethod());

        if ($scheme->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
    }

    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function showById($id)
    {
        // $scheme = new Scheme;
        // $scheme = Scheme::find(1);
        // // echo($id);
        // // var_dump($scheme);
        // return response()->json($result);
        return response()->json(Scheme::findOrFail($id));
    }
    
    /**
     * Retrieve the all user 
     *
     * @return Response
     */
    public function showAllScheme(Request $request)
    {
        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            die("Could not connect to the database.  Please check your configuration.");
        }
        return response()->json(Scheme::all());
    }

    public function showAllSchemeName(){
        $scheme = DB::table('competition_scheme')->select('schemeName')->get();
        
        if (!is_null($scheme)){
            return response()->json($scheme);
        }else{
            return response()->json(['status' => 'data scheme kosong']);
        }
        
    }

    /**
     * Update the user for the given ID.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $scheme = Scheme::findOrFail($id);
        $scheme->schemeName = $request->input('schemeName');
        
        print($request->getMethod());

        if ($scheme->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
        
        // return response()->json($scheme, 200);
    }

    /**
     * Delete the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        Scheme::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}

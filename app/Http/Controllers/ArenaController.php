<?php

namespace App\Http\Controllers;

use App\Arena;
use App\Helper\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArenaController extends Controller
{
    
    
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $arena = new Arena;

        $arena->arenaName = $request->input('arenaName');
        $arena->arenaId = $request->input('arenaId');
        print($request->getMethod());

        if ($arena->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
    }

    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function showById($id)
    {
        // $arena = new Arena;
        // $arena = Arena::find(1);
        // // echo($id);
        // // var_dump($arena);
        // return response()->json($result);
        return response()->json(Arena::findOrFail($id));
    }
    
    /**
     * Retrieve the all user 
     *
     * @return Response
     */
    public function showAllArena(Request $request)
    {
      
        return response()->json(Arena::all());
    }

    /**
     * Update the user for the given ID.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $arena = Arena::findOrFail($id);
        $arena->arenaName = $request->input('arenaName');
        
        print($request->getMethod());

        if ($arena->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
        
        // return response()->json($arena, 200);
    }

    /**
     * Delete the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        Arena::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}

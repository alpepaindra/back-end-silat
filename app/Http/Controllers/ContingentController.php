<?php

namespace App\Http\Controllers;

use App\Contingent;
use App\Official;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ContingentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $contingent = new Contingent;

        $contingent->contingentName = $request->json()->get('contingentName');
        $contingent->contingentAddress = $request->json()->get('contingentAddress');
        $contingent->participantAmount = $request->json()->get('participantAmount');
        $contingent->officialId = $request->json()->get('officialId');
        
       
       // var_dump($request);

        
        if ($contingent->save()){
            $insert = array('championshipId' => $request->json()->get('championship'), 'contingentId' => $contingent->contingentId);
            DB::table('bagian')->insert($insert);
            return response()->json(['status' => 'success', 'contingentId' => $contingent->contingentId]);
        }else{
            return response()->json(['status' => 'fail']);
        }
    }

    public function save(Request $request){
        $official = new Official;

        $official->officialName = $request->json()->get('officialName');
        $contingent = new Contingent;

        $contingent->contingentName = $request->json()->get('contingentName');
        $contingent->contingentAddress = $request->json()->get('contingentAddress');
        $contingent->participantAmount = $request->json()->get('participantAmount');
        
        // print_r($official->officialName);
        $bool = 0;
        $contingentName = DB::table('contingent')->select('contingentName')->get();
        
        foreach ($contingentName as $contingentNames) {
            $contingentNames->contingentName = strtoupper($contingentNames->contingentName);
            $contingent->contingentName = strtoupper($contingent->contingentName);

            if ($contingentNames->contingentName == $contingent->contingentName){
                $bool = 1;
            }
        }
        if($bool == 0){
            if ($contingent->save() && $official->save()){
                $contingent->officialId = $official->officialId;
                $official->contingentId = $contingent->contingentId;
                if ($contingent->save() && $official->save()){
                    $insert = array('championshipId' => $request->json()->get('championshipId'), 'contingentId' => $contingent->contingentId);
                    DB::table('bagian')->insert($insert);
                    return response()->json(['status' => 'success', 'contingentId' => $contingent->contingentId]);
                }
            }else{
                return response()->json(['status' => 'fail']);
            }
        }else{
            return response()->json(['status' => 'fail', 'message' => "there is same contingent Name"]);
        }

    }

    public function showContingentBy(Request $request){
        $name = $request->input('name');
        $officialName = $request->input('officialName');
        $amount = $request->input('amount');
        $address = $request->input('address');
        $id = $request->input('id');
        $data =  DB::table('contingent')
        ->select('contingent.*', 'official.officialName')
        ->leftJoin('official', 'contingent.officialId', '=', 'official.officialId')
        ->leftjoin('bagian', 'contingent.contingentId', '=', 'bagian.contingentId')
        ->where('bagian.championshipId', $request->input('championship'));

        if($name != null){
            $data = $data->where('contingent.contingentName', 'like', "%".$name."%");
        }
        if($officialName != null){
            $data = $data->where('official.officialName', 'like', "%".$officialName."%");
        }
        if($amount !=0){
            $data = $data->where('contingent.participantAmount', $amount);
        }
        if($address != null){
            $data = $data->where('contingent.contingetnAddress', 'like', "%".$address."%");
        }
        if($id != 0){
            $data = $data->where('contingent.contingentId', $id);
        }
        $data = $data->get();
        
        return response()->json($data);

    }

    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function showById($id, $championship)
    {
        $contingent = DB::table('contingent')->select('contingent.*','official.officialName')
        ->leftJoin('official', 'contingent.contingentId', '=', 'official.contingentId')
        ->leftJoin('bagian', 'contingent.contingentId', '=', 'bagian.contingentId')
        ->leftJoin('championship', 'bagian.championshipId', '=', 'championship.championshipId')
        ->where('bagian.championshipId', $championship)
        ->where('contingent.contingentId', $id)
        ->first();

        // $contingent1 = Contingent::findOrFail($id)->leftJoin('official', 'contingent.contingentId', '=', 'official.contingentId');
        // return response()->json(DB::table('contingent')
        // ->leftJoin('official', 'contingent.contingentId', '=', 'official.contingentId')
        // ->where('contingent.contingentId', $id)
        // ->get());
        return response()->json($contingent);
    }

    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function showByChampionshipId($id)
    {
        $contingent = DB::table('contingent')->select('contingent.*', 'official.officialName')
        ->leftJoin('bagian', 'contingent.contingentId', '=', 'bagian.contingentId')
        ->leftJoin('official', 'contingent.officialId', '=', 'official.officialId')
        ->leftJoin('championship', 'bagian.championshipId', '=', 'championship.championshipId')
        ->where('bagian.championshipId', $id)
        ->get();
        return response()->json($contingent);
    }

    /**
     * Retrieve the all user 
     *
     * @return Response
     */
    public function showAllContingent()
    {
        $contingent = DB::table('contingent')
        ->select('contingent.contingentId', 'contingent.contingentName', 'contingent.participantAmount', 'contingent.contingentAddress', 'official.officialName')
        ->leftJoin('official', 'contingent.contingentId', '=', 'official.contingentId')
        ->leftJoin('bagian','contingent.contingentId', '=', 'bagian.contingentId')
        ->where('bagian.championshipId', $request->input('championship'))
        ->get();
        return response()->json($contingent);
        // return response()->json(Contingent::select('contingentName', 'participantAmount')->with(['official' => function($query) {
        //     $query->select('officialId','officialName');
        // }])->get());
    }

    public function getContingentMedal(Request $request){
        $contingent = DB::table('contingent')
        ->select('contingent.contingentName', 'contingent.totalGoldMedal', 'contingent.totalSilverMedal', 'contingent.totalBronzeMedal', 'contingent.totalMedal', 'bagian.*')
        ->leftJoin('bagian','contingent.contingentId', '=', 'bagian.contingentId')
        ->where('bagian.championshipId', $request->input('championship'))
        ->orderBy('totalMedal', 'dsc')
        ->get();
        return response()->json($contingent);
    }

    /**
     * Update the user for the given ID.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $contingent = Contingent::findOrFail($id);
        $contingent->contingentName = $request->json()->get('contingentName');
        $contingent->participantAmount = $request->json()->get('participantAmount');
        $contingent->contingentAddress = $request->json()->get('contingentAddress');
        $official = Official::findOrFail($contingent->officialId);
        $official->officialName = $request->json()->get('officialName');
        $official->username = $request->json()->get('username');
        $official->password = $request->json()->get('password');
        
        if ($contingent->save() && $official->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
        

        // return response()->json($contingent, 200);
    }

    /**
     * Delete the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        Contingent::findOrFail($id)->delete();
        DB::table('participant')->where('contingentId', $id)->delete();
        return response('Deleted Successfully', 200);
    }
}

<?php

namespace App\Http\Controllers;

use App\GrandJuror;
use App\Helper\Message;
use Illuminate\Http\Request;

class GrandJurorController extends Controller
{
    
    
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $grandJuror = new GrandJuror;

        $grandJuror->grandJurorName = $request->json()->get('grandJurorName');
        $grandJuror->championshipId = $request->json()->get('championshipId');
        $grandJuror->role = $request->json()->get('role');
        $grandJuror->gender = $request->json()->get('gender');

        
        if ($grandJuror->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
    }

    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function showById($id)
    {
        // $grandJuror = new GrandJuror;
        // $grandJuror = GrandJuror::find(1);
        // // echo($id);
        // // var_dump($grandJuror);
        // return response()->json($result);
        return response()->json(GrandJuror::findOrFail($id));
    }
    
    /**
     * Retrieve the all user 
     *
     * @return Response
     */
    public function showAllGrandJuror(Request $request)
    {
        return response()->json(GrandJuror::where('championshipId', $request->input('championshipId'))->get());
    }

    /**
     * Update the user for the given ID.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $grandJuror = GrandJuror::findOrFail($id);
        $grandJuror->grandJurorName = $request>json()->get('grandJurorName');
        $grandJuror->role = $request>json()->get('role');
        $grandJuror->gender = $request>json()->get('gender');

        
        if ($grandJuror->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
    }

    /**
     * Delete the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        GrandJuror::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}

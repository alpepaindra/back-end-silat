<?php

namespace App\Http\Controllers;

use App\Juror;
use App\Helper\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JurorController extends Controller
{
    
    
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $juror = new Juror;

        $juror->jurorName = $request->json()->get('jurorName');
        $juror->championshipId = $request->json()->get('championshipId');
        $juror->role = $request->json()->get('role');
        $juror->gender = $request->json()->get('gender');

        if ($juror->save()){
            // DB::table('scoring')->insert(['jurorId' => $juror->jurorId, 'competitionId' => $request->input('competitionId')]);
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
    }

    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function showById($id)
    {
        // $juror = new Juror;
        // $juror = Juror::find(1);
        // // echo($id);
        // // var_dump($juror);
        // return response()->json($result);
        return response()->json(Juror::findOrFail($id));
    }
    
    /**
     * Retrieve the all user 
     *
     * @return Response
     */
    public function showAllJuror(Request $request)
    {
        return response()->json(Juror::where('championshipId', $request->input('championshipId'))->get());
    }

    /**
     * Retrieve the all user 
     *
     * @return Response
     */
    public function showCompetitionJuror($id){
        $juror = DB::table('juror')
        ->leftJoin('scoring', 'juror.jurorid', '=', 'scoring.jurorId')
        ->where('scoring.competitionId', $id)
        ->get();
        return response()->json($juror);
    }

    /**
     * Update the user for the given ID.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $juror = Juror::findOrFail($id);
        $juror->jurorName = $request>json()->get('jurorName');
        $juror->role = $request>json()->get('role');
        $juror->gender = $request>json()->get('gender');

        if ($juror->save()){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'fail']);
        }
        
        // return response()->json($juror, 200);
    }

    /**
     * Delete the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        Juror::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}

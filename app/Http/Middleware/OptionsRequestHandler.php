<?php

namespace App\Http\Middleware;

class OptionsRequestHandler{

    public function register()
  {
    $request = app('request');
    // echo('1');
    print_r($request->getMethod());
    if ($request->isMethod('OPTIONS'))
    {
      app()->options($request->path(), function() { return response('', 200); });
    }
  }
}
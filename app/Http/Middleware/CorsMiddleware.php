<?php 
namespace App\Http\Middleware;

class CorsMiddleware {

    public function handle($request, \Closure $next)
  {
    $response = $next($request);
    $response->header('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE, OPTIONS');
    $response->header('Access-Control-Allow-Headers', $request->header('Access-Control-Request-Headers'));
    $response->header('Access-Control-Allow-Origin', '*');
    return $response;
  }
//   }
//   public function handle($request, Closure $next)
//   { 
//   $headers = [
//     'Access-Control-Allow-Origin' => '*',
//        'Access-Control-Allow-Methods'=> 'POST, GET, OPTIONS, PUT, DELETE',
//        'Access-Control-Allow-Headers'=> 'Content-Type, X-Auth-Token, Origin'
//    ];
//   //  echo('3');
//   //  print_r($request->getMethod());
// if($request->getMethod() == "OPTIONS") {
//        // The client-side application can set only headers allowed in Access-Control-Allow-Headers
//        return Response::make('OK', 200, $headers);
//    }

//    $response = $next($request);
//    foreach($headers as $key => $value) 
//     $response->header($key, $value);
// return $response;
// }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    protected $table = 'participant';
    public $timestamps = false;
    protected $primaryKey = 'participantId';

/**
    * Retrieve the user for the given ID.
    *
    * @param  string  $name
    */
    // public function showByName($name){
    //     $participant = DB::table('participant')->where('gender', $name);
    //     return $participant;
    // }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Championship extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    protected $table = 'championship';
    public $timestamps = false;
    protected $primaryKey = 'championshipId';
}
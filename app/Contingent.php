<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contingent extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    protected $table = 'contingent';
    public $timestamps = false;
    protected $primaryKey = 'contingentId';

    public function official(){
        return $this->hasMany('App\Official', 'contingentId');
    }
}